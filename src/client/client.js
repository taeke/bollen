import { Connector } from "./connector.js";
import { DataType, Data } from "../common/data.js";
import { PlayerState, Status } from "../common/playerstate.js";
import { Enum } from "../common/enum.js";

/* 
Class for communication between view and connector. 
*/
class Client {
    constructor(view, serverId) {
        this.sendPingTimer = undefined;
        this.expectPongTimer = undefined;
        this.playerState = undefined;
        this.serverId = serverId;
        this.view = view;
        this.view.onPlayerWantsToJoin.addListener((name) => this.join(name));
        this.view.onPlayedCard.addListener((card) => this.connector.sendToConnection(new Data(DataType.PlayCard, undefined, card)));
        this.view.onPlaceBid.addListener((bid) => this.connector.sendToConnection(new Data(DataType.PlaceBid, undefined, bid)));

        this.connector = new Connector();
        this.connector.onCanJoinEvent.addListener(() => this.handleCanJoin());
        this.connector.onReceivedDataEvent.addListener((data) => this.handleData(data));
        this.connector.onErrorEvent.addListener(() => this.handleCantConnect());
    }

    run() {
        this.connector.run(this.serverId);
    }

    reset() {
        if(this.sendPingTimer) window.clearTimeout(this.sendPingTimer);
        if(this.expectPongTimer) window.clearTimeout(this.expectPongTimer);
    }

    join(name) {
        this.connector.join(name);
    }

    handleCantConnect() {
        this.view.showError("showServerNotAvailable");
        this.view.showStatus(Status.CantJoin);
    }

    handleCanJoin() {
        if(!this.playerState) this.view.showJoinElement();
    }

    handleData(data) {
        const dataType = Enum.fromJson(DataType, data.dataType);
        this.reset();
        switch (dataType) {
            case DataType.PlayerState:
                this.playerState = PlayerState.fromJson(data.content);
                this.handlePlayerState();
                break;
            case DataType.TooMany:
                this.view.hideJoinElement();
                this.view.showStatus(Status.CantJoin);
                this.view.showError("tooMany");
                break;
            case DataType.Pong:
                this.setPingTimeouts();
                break;
            default:
                this.view.hideJoinElement();
                this.view.showStatus(Status.Error);
                this.view.showError("somethingWrong"); 
        }
    }

    setPingTimeouts() {
        this.sendPingTimer = setTimeout(() => this.connector.sendPing(), bollenConfig.pingTimeOut);
        this.expectPongTimer = setTimeout(() => this.handleLostConnection(), bollenConfig.pingResponseTimeOut);
    }

    handleLostConnection() {       
        this.view.showStatus(Status.Disconnected);
        this.view.showError("lostConnection");
    }

    handlePlayerState() {
        const status = Object.values(Status).find(status => status.name == this.playerState.status.name);
        this.view.showStatus(status);
        this.view.showPlayers(this.playerState.playerList, status)
        this.view.hideJoinElement();
        switch (status) {
            case Status.Waiting:
                break;
            case Status.GameStarted:
                this.view.draw(this.playerState);
                this.showTurn();
                break;
            default:
                this.view.showStatus(Status.Error);
                this.view.showError("somethingWrong");
        }
    }

    showTurn() {
        let playerWhosTurnItIs = this.playerState.playerList.find(player => player.itsYourTurnToPlaceBid || player.itsYourTurnToPlayACard);
        if(playerWhosTurnItIs) {
            this.view.showMessage("turn", playerWhosTurnItIs.name); 
        }
    }
}

export { Client };
