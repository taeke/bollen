import { State } from "./state.js";

/*
Class for storing and retrieving connection information from local storage.
*/
class Storage {
    constructor() {
        this.storageName = "clientV1";
    }

    getClientState(serverId) {
        return this.getState().getClientState(serverId);
    }

    store(clientState) {
        const state = this.getState();
        state.addClientState(clientState);
        localStorage.setItem(this.storageName, JSON.stringify(state));
    }

    getState() {
        let state = Object.assign(new State, JSON.parse(localStorage.getItem(this.storageName)));
        if(state) return state;
        return new State();        
    }
}

export { Storage };
