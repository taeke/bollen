/*
Classes for holding connection information.
*/
class ClientState {
    constructor(serverId, clientId, name) {
        this.serverId = serverId;
        this.clientId = clientId;
        this.name = name;
    }
}

class State {
    constructor() {
        this.clientStateList = [];
    }

    getClientState(serverId) {
        return this.clientStateList.find((clientState) => clientState.serverId == serverId);
    }

    addClientState(clientState) {
        this.clientStateList.push(clientState);
    }
}

export { State, ClientState };
