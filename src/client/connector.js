import { Peer } from "peerjs";
import { Storage } from "./storage.js";
import { ClientState } from "./state.js";
import { Event } from "../common/event.js";
import { Data, DataType } from "../common/data.js";

/* 
Class for setting up a connection with the server and sending and receiving data.
*/
class Connector {
    constructor() {
        this.peer = undefined;
        this.connection = undefined;
        this.serverId = undefined;
        this.clientId = undefined;

        this.storage = new Storage();

        this.onCanJoinEvent = new Event();
        this.onReceivedDataEvent = new Event();
        this.onErrorEvent = new Event();
    }

    run(serverId) {
        this.serverId = serverId;
        const clientState = this.storage.getClientState(serverId);
        this.peer = new Peer(clientState?.clientId);
        this.peer.on("open", (clientId) => this.peerOpen(clientId, clientState));
        this.peer.on("error", (error) => this.onPeerError(error));
    }

    onPeerError(error) {
        if (error.type = "peer-unavailable") {
            const length = "Could not connect to peer ".length;
            const id = error.message.substring(length);
            this.onErrorEvent.trigger(id);
        } else {
            console.error(error);
        }
    }

    peerOpen(clientId, clientState) {
        this.clientId = clientId;
        if(clientState) {
            this.join(clientState.name);
        } else {
            this.onCanJoinEvent.trigger();
        }
    }

    join(name) {
        this.storage.store(new ClientState(this.serverId, this.clientId, name));

        this.connection = this.peer.connect(this.serverId, { label: name });
        this.connection.on("data", (data) => this.onReceivedDataEvent.trigger(data));
        this.connection.on("error", (error) => console.error(error));
    }

    sendPing() {
        this.sendToConnection(new Data(DataType.Ping, this.clientId, this.clientId));
    }

    sendToConnection(data) {
        data.clientId = this.clientId;
        if (this.connection.open) {
            this.connection.send(data);
        } else {
            this.connection.on("open", function () {
                this.connection.send(data);
            });
        }
    }
}

export { Connector };
