/*
Class for base functionality of drawing on a canvas.
*/
class Draw {
    constructor(context, size) {
        this.context = context;
        this.size = size;
    }

    draw(point, drawFunction) {
        this.context.save();
        this.context.beginPath();
        this.context.translate(point.x, point.y);
        this.context.textAlign = "center";
        this.context.textBaseline = 'middle';
        this.context.font = "bold " + this.size.fontSize + "px sans-serif";
        drawFunction.call(this);
        this.context.restore();
    }
}

export { Draw };
