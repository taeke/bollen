import { Suit } from "../common/deck.js";

/*
Classes for drawing a a suit on the canvas.
*/

class SuitDrawing {
    constructor(context, size) {
        this.context = context;
        this.size = size;
        this.suitPathList = this.getSuitPathList();
        this.suitColorList = this.getSuitColorList();
    }

    draw(suit, point, scale) {
        this.context.save();
        this.context.beginPath();
        this.context.translate(point.x, point.y);
        this.drawShape(suit, scale);
        this.context.restore();       
    }

    drawShape(suit, scale) {
        const halfPath = this.suitPathList.get(suit)(this.size.base * scale);
        this.context.fillStyle = this.suitColorList.get(suit);
        this.context.moveTo(halfPath[0][0], halfPath[0][1]);
        for (let i = 0; i < halfPath.length; i++) {
            let path = halfPath[i];
            this.context.bezierCurveTo(path[1][0], path[1][1], path[2][0], path[2][1], path[3][0], path[3][1]);
        }

        for (let i = 0; i < halfPath.length; i++) {
            let path = halfPath[halfPath.length - i - 1];
            this.context.bezierCurveTo(-path[2][0], path[2][1], -path[1][0], path[1][1], -path[0][0], path[0][1]);
        }

        this.context.fill(); 
    }

    getSuitColorList() {
        const suitColorList = new Map();
        suitColorList.set(Suit.Diamonds, "red");
        suitColorList.set(Suit.Clubs, "black");
        suitColorList.set(Suit.Hearts, "red");
        suitColorList.set(Suit.Spades, "black");
        return suitColorList;
    }

    getSuitPathList() {
        const suitPathList = new Map();
        suitPathList.set(Suit.Diamonds, (size) => [[[0, size], [0, size], [3*size/4, 0], [3*size/4, 0]], [[3*size/4, 0], [3*size/4, 0], [0, -size], [0, -size]]]);
        suitPathList.set(Suit.Clubs, (size) => [[[0, -size], [0, -size], [size/2, -size], [size/2, -size/2]], [[size/2, -size/2], [size/2, -size/2], [size, -size/2], [size, 0]], [[size, 0], [size, 0], [size, size/2], [size/2, size/2]], [[size/2, size/2], [size/2, size/2], [size/8, size/2], [size/8, size/8]],[[size/8, size/8], [size/8, size/2], [size/2, size], [size/2, size]], [[size/2, size], [size/2, size], [0, size], [0,size]]]);
        suitPathList.set(Suit.Hearts, (size) => [[[0, size], [0, size], [size, 0], [size, -size/2]], [[size, -size/2], [size, -size/2], [size, -size], [size/2, -size]], [[size/2, -size], [size/2, -size], [0, -size], [0, -size/2]]]);
        suitPathList.set(Suit.Spades, (size) => [[[0, -size], [0, -size], [size, -size/2], [size, 0]], [[size, 0], [size, 0], [size, size/2], [size/2, size/2]], [[size/2, size/2], [size/2, size/2], [size/8, size/2], [size/8, size/8]], [[size/8, size/8], [size/8, size/2], [size/2, size], [size/2, size]], [[size/2, size], [size/2, size], [0, size], [0,size]]]);
        return suitPathList;
    }
}

export { SuitDrawing };
