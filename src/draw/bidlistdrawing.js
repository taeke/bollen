/*
classes for drawing the bids the player can choose from.
*/

class BidDrawing {
    constructor(context, size) {
        this.context = context;
        this.size = size;
    }

    draw(point) {
        this.context.save();
        this.context.beginPath();
        this.context.lineWidth = 2;
        this.context.strokeStyle = "black"; 
        this.context.translate(point.x, point.y);
        this.context.moveTo(this.size.bidListSize.radius, 0);
        this.context.arc(0, 0, this.size.bidListSize.radius, 0, 2 * Math.PI);
        this.context.stroke();  
        this.context.restore();
    }
    
    drawText(point, bid) {
        this.context.save();
        this.context.beginPath();
        this.context.translate(point.x, point.y);
        this.context.fillStyle = "black"; 
        this.context.textAlign = "center";
        this.context.textBaseline = 'middle';
        this.context.font = "bold " + this.size.fontSize + "px sans-serif";
        this.context.fillText(bid, 0, 0);
        this.context.restore();
    }
}

class BidListDrawing {
    constructor(context, size) {
        this.possibleBids = undefined;
        this.context = context;
        this.size = size;
        this.bidDrawing = new BidDrawing(this.context, size);
    }

    draw(itsYourTurnToPlaceBid, possibleBids) {
        this.possibleBids = possibleBids;
        this.slots = this.size.bidListSize.getSlots(possibleBids);
        if(itsYourTurnToPlaceBid) {
            this.points = this.size.bidListSize.getPoints(possibleBids);
            for (let index = 0; index < possibleBids.length; index++) {
                this.bidDrawing.draw(this.points[index]);
            }

            for (let index = 0; index < possibleBids.length; index++) {
                this.bidDrawing.drawText(this.points[index], possibleBids[index]);
            }
        }
    }

    selectBid(point) {
        let bid = undefined;
        for (let index = 0; index < this.slots.length; index++) {
            if(this.slots[index].isInside(point)) {
                bid = this.possibleBids[index];
            }
        }

        return bid;
    }
}

export { BidDrawing, BidListDrawing};
