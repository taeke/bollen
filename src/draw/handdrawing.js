import { CardDrawing } from "./carddrawing.js";
import { Distance } from "../canvas/rectangle.js"

/*
Classes for drawing the cards the player is holding in his hand.
*/

class DragingStates {
    static NotYourTurn = new DragingStates("NotYourTurn");
    static NotSelected = new DragingStates("NotSelected");
    static Selected = new DragingStates("Selected");
    static Dropped = new DragingStates("Dropped");

    constructor(name) {
        this.name = name;
    }
}

class HandDrawing {
    constructor(context, size) {
        this.context = context;
        this.size = size;
        this.cardDrawing = new CardDrawing(context, size);
        this.cards = undefined;
        this.slots = [];
        this.selectedCard = undefined;
        this.draggingDistance = new Distance(0, 0);
    }

    draw(cards, draggingState) {
        this.cards = cards;
        this.internalDraw(draggingState);
    }

    internalDraw(draggingState) {
        this.slots = this.size.handSize.getSlots(this.cards.length);
        this.cards.forEach(card => {
            this.drawCard(card, draggingState);
        });
    }

    drawCard(card, draggingState) {
        if(card == this.selectedCard) {
            if(draggingState == DragingStates.Selected) {
                this.cardDrawing.draw(this.getNewPoint(), card);
            }
        } else {
            if(card) {
                this.cardDrawing.draw(this.slots[this.getIndexCard(card)].topLeftPoint, card);
            }
        }
    }

    getIndexCard(cardToFind) {
        return this.cards.findIndex(card => card == cardToFind);
    }

    selectCard(point) {
        this.selectedCard = undefined;
        for (let index = 0; index < this.slots.length; index++) {
            if(this.slots[index].isInside(point)) {
                this.selectedCard = this.cards[index];
            }
        }
    }

    getNewPoint() {
        let point = undefined;
        this.cards.forEach(card => {
            if(card == this.selectedCard && this.draggingDistance) {
                point = this.slots[this.getIndexCard(card)].topLeftPoint.plusDistance(this.draggingDistance);
            }
        }); 
        
        return point;
    }
}

export { DragingStates, HandDrawing };
