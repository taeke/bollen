import { RankDrawing } from "./rankdrawing.js";

/*
Class for drawing a playing card on the canvas.
*/
class CardDrawing {
    constructor(context, size) {
        this.context = context;
        this.size = size;
        this.rankDrawing = new RankDrawing(context, size);
    }
    
    draw(point, card) {
        if(point) {
            this.drawShape(point);
            this.rankDrawing.drawShape(card, point);
            this.rankDrawing.drawText(card, point);
        } 
    }

    drawShape(point) {
        this.context.save();
        this.context.beginPath();
        this.context.translate(point.x, point.y);
        const points = this.size.cardSize.getPoints();
        this.context.moveTo(points[1].x, points[1].y);
        this.context.arcTo(points[2].x, points[2].y, points[3].x, points[3].y, this.size.cardSize.radius);
        this.context.arcTo(points[3].x, points[3].y, points[4].x, points[4].y, this.size.cardSize.radius);
        this.context.arcTo(points[4].x, points[4].y, points[0].x, points[0].y, this.size.cardSize.radius);
        this.context.arcTo(points[0].x, points[0].y, points[2].x, points[2].y, this.size.cardSize.radius);
        this.context.fillStyle = "#EEEEE4";
        this.context.fill();
        this.context.lineWidth = 2;
        this.context.strokeStyle = "black";  
        this.context.stroke();
        this.context.restore();
    }
}

export { CardDrawing };
