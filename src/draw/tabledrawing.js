import { PlayerDrawing } from "./playerdrawing.js";


class TableDrawing {
    constructor(context, size) {
        this.context = context;
        this.size = size;
        this.playerDrawing = new PlayerDrawing(context, size);
        this.droppedCard = undefined;
    }

    draw(playerList, draggingState) {
        const playerPointList = this.size.table.getPlayerPointList();
        for (let index = 0; index < playerList.length; index++) {
            this.playerDrawing.draw(playerPointList[index], playerList[index], this.droppedCard, draggingState);
        }
    }

    canDrop(playerList, newPoint) {
        let can = false;
        this.playerList = playerList;
        this.snapZones = this.size.table.getSnapZones();
        for (let index = 0; index < this.playerList.length; index++) {
            if(playerList[index].itsYourTurnToPlayACard) {
                can = this.snapZones[index].isInside(newPoint);
            };
        }

        return can;
    }
}

export { TableDrawing };
