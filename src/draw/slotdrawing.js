class SlotDrawing {
    constructor(context, size) {
        this.context = context;
        this.size = size;
    }

    draw(point) {
        this.context.save();
        this.context.beginPath();
        this.context.translate(point.x, point.y);
        this.drawShape();
        this.context.restore(); 
    }

    drawShape() {
        this.context.lineWidth = 2;
        this.context.setLineDash([this.size.base, this.size.base]);
        this.context.moveTo(0,0);
        this.context.lineTo(this.size.cardSize.width, 0);
        this.context.lineTo(this.size.cardSize.width, this.size.cardSize.height);
        this.context.lineTo(0, this.size.cardSize.height);
        this.context.lineTo(0, 0);
        this.context.stroke();
    }
}

export { SlotDrawing };
