import { Draw } from "./draw.js";
import { CardDrawing } from "./carddrawing.js";
import { DragingStates } from "./handdrawing.js";
import { SuitDrawing } from "./suitdrawing.js";
import { Point } from "../canvas/rectangle.js";

class PlayerDrawing extends Draw {
    constructor(context, size) {
        super(context, size);
        this.player = undefined;
        this.cardDrawing = new CardDrawing(context, size);
        this.suitDrawing = new SuitDrawing(context, size);
    }

    draw(point, player, droppedCard, draggingState) {
       this.player = player;
       this.droppedCard = droppedCard;
       this.draggingState = draggingState;
       super.draw(point, this.internalDraw) 
    }

    internalDraw() {
        super.draw(this.size.player.getNamePoint(), this.drawName);
        const bollenPointList = this.size.player.getBollenPointList(this.player.numberOfBollen);
        bollenPointList.forEach(point => {
            super.draw(point, this.drawBol);
        });

        if(this.player.previousCard) {
            super.draw(this.size.player.getPreviousCardSuitPoint(), this.drawPreviousCardSuit);
            super.draw(this.size.player.getPreviousCardRankPoint(), this.drawPreviousCardRank);
        }


        if(this.player.cardOnTable || this.droppedCard) {
            super.draw({x:0, y:0}, this.drawCard);
        } else {
            if((this.draggingState == DragingStates.NotSelected || this.draggingState == DragingStates.Selected) && this.player.itsYourTurnToPlayACard) {
                super.draw(this.size.player.getCardPoint(), this.drawDropZone)
            }              
        }
    }

    drawName() {
        let name = this.player.name;
        if(this.player.bidCurrentRound !== null) {
            name = name + " " + this.player.bidCurrentRound + "/" + this.player.scoreCurrentRound;
        }

        this.context.fillText(name, 0, 0);
    }

    drawBol() {
        this.context.arc(0, 0, this.size.player.bolSize, 0, 2 * Math.PI);
        this.context.fill();  
    }

    drawCard() {
        const card = this.player.cardOnTable ? this.player.cardOnTable : this.droppedCard;
        this.cardDrawing.draw(this.size.player.getCardPoint(), card);
    }

    drawPreviousCardSuit() {
        this.suitDrawing.draw(this.player.previousCard.suit, new Point(0, 0), 1);
    }

    drawPreviousCardRank() {
        this.context.fillText(this.player.previousCard.rank.shortName, 0 , 0);
    }

    drawDropZone() {
        this.context.lineWidth = 2;
        this.context.setLineDash([this.size.base, this.size.base]);
        this.context.rect(0, 0, this.size.cardSize.width, this.size.cardSize.height);
        this.context.stroke();
    }
}

export { PlayerDrawing };
