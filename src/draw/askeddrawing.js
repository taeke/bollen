import { SuitDrawing } from "./suitdrawing.js";

class AskedDrawing {
    constructor(context, size) {
        this.context = context;
        this.size = size;
        this.suitDrawing = new SuitDrawing(context, size);
    }

    draw(asked) {
        if(asked) {
            this.suitDrawing.draw(asked, this.size.askedSize.getPointSuit(), 1);
            this.drawText(this.size.askedSize.getPointText())
        }
    }

    drawText(point) {
        this.context.save();
        this.context.beginPath();
        this.context.textAlign = "center";
        this.context.textBaseline = 'middle';
        this.context.font = "bold " + this.size.fontSize + "px sans-serif";
        this.context.fillText(bollenConfig.translator.translate("asked"),  point.x, point.y);
        this.context.restore();  
    }
}

export { AskedDrawing };