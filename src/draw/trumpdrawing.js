import { SuitDrawing } from "./suitdrawing.js";

class TrumpDrawing {
    constructor(context, size) {
        this.context = context;
        this.size = size;
        this.suitDrawing = new SuitDrawing(context, size);
    }

    draw(trump) {
        if(trump) {
            this.suitDrawing.draw(trump, this.size.trumpSize.getPointSuit(), 1);
            this.drawText(this.size.trumpSize.getPointText())
        }
    }

    drawText(point) {
        this.context.save();
        this.context.beginPath();
        this.context.textAlign = "center";
        this.context.textBaseline = 'middle';
        this.context.font = "bold " + this.size.fontSize + "px sans-serif";
        this.context.fillText(bollenConfig.translator.translate("trump"),  point.x, point.y);
        this.context.restore();  
    }
}

export { TrumpDrawing };