import { AskedDrawing } from "./askeddrawing.js"

const askedDrawing = new AskedDrawing();

import { BidDrawing, BidListDrawing} from "./bidlistdrawing.js"

const bidDrawing = new BidDrawing();

const bidListDrawing = new BidListDrawing();

import { CardDrawing } from "./carddrawing.js";

const cardDrawing = new CardDrawing();

import { DragingStates, HandDrawing } from "./handdrawing.js";

const dragingStates = new DragingStates();

const handDrawing = new HandDrawing();

import { RankDrawing } from "./rankdrawing.js";

const rankDrawing = new RankDrawing();

import { SlotDrawing } from "./slotdrawing.js";

const slotDrawing = new SlotDrawing();

import { SuitDrawing } from "./suitdrawing.js";

const suitDrawing = new SuitDrawing();

import { TrumpDrawing } from "./trumpdrawing.js";

const trumpDrawing = new TrumpDrawing();

import { Draw } from "./draw.js";

const canvasElement = document.createElement("canvas");
const canvasContext = canvasElement.getContext('2d');
document.body.appendChild(canvasElement);

class TestDraw extends Draw {
    draw(point) {
        super.draw(point, this.internalDraw);
    }

    internalDraw() {
        this.context.rect(0,0,50,50);
        this.context.stroke();
        this.context.fillText("Test", 0, 0);
    }
}

const draw = new TestDraw(canvasContext, { fontSize: 30 });
console.log("Should draw a rectangle and Text centered topleft corner of rectangle.")
draw.draw({x:30, y:30});

import { PlayerDrawing } from "./playerdrawing.js";

const canvasElement2 = document.createElement("canvas");
canvasElement2.width = 800;
canvasElement2.height = 900;
const canvasContext2 = canvasElement2.getContext('2d');
document.body.appendChild(canvasElement2);

import { PlayerSize } from "../size/playersize.js";

import { Player, Status } from "../common/playerState.js";
import { Card, Rank, Suit } from "../common/deck.js";
import { Size } from "../size/size.js";
import { TableDrawing } from "./tabledrawing.js";
import { TableSize } from "../size/tablesize.js";
import { Point } from "../canvas/rectangle.js";

const player = new Player("1", "test", true);
player.numberOfBollen = 3;
//player.cardOnTable = new Card(Rank.Eight, Suit.Diamonds);
player.itsYourTurnToPlayACard = true;
player.bidCurrentRound = 2;
player.scoreCurrentRound = 1;

const size = new Size(canvasElement2);
size.calculate();
size.fontSize = 20;

const playerDrawing = new PlayerDrawing(canvasContext2, size);

playerDrawing.draw({x:0, y:0}, player, new Card(Rank.Eight, Suit.Clubs), DragingStates.Selected);


const canvasElement3 = document.createElement("canvas");
canvasElement3.width = 800;
canvasElement3.height = 900;
const canvasContext3 = canvasElement3.getContext('2d');
document.body.appendChild(canvasElement3);

const tableDrawing = new TableDrawing(canvasContext3, size);

const player2 = new Player("2", "piet", true);
player2.numberOfBollen = 7;
player2.cardOnTable = new Card(Rank.Eight, Suit.Clubs);
player2.itsYourTurnToPlayACard = true;
player2.bidCurrentRound = 0;
player2.scoreCurrentRound = 0;

const player3 = new Player("3", "klaas", true);
player3.numberOfBollen = 1;
player3.cardOnTable = new Card(Rank.Nine, Suit.Hearts);
player3.itsYourTurnToPlayACard = true;
player3.bidCurrentRound = 0;
player3.scoreCurrentRound = 1;

tableDrawing.draw([player, player2, player3], DragingStates.Selected);

console.log(tableDrawing.canDrop([player, player2, player3], new Point(100, 100)));
