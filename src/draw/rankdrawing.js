import { Rank } from "../common/deck.js";
import { SuitDrawing } from "./suitdrawing.js";

/*
Classes for drawing a a rank on the canvas.
*/

class RankCellList {
    constructor(cellList, value) {
        this.cellList = cellList;
        this.value = value;
    }
}

class Cell {
    constructor(row, column) {
        this.row = row;
        this.column = column;
    }
}

class RankDrawing {
    constructor(context, size) {
        this.context = context;
        this.size = size;
        this.suitDrawing = new SuitDrawing(context, size);
        this.rankListCellList = new Map();
        this.rankListCellList.set(Rank.King, new RankCellList([new Cell(0,0), new Cell(0,2), new Cell(7,0), new Cell(7,2)], "K"));
        this.rankListCellList.set(Rank.Queen, new RankCellList([new Cell(0,0), new Cell(0,2), new Cell(7,0), new Cell(7,2)], "Q"));
        this.rankListCellList.set(Rank.Jack, new RankCellList([new Cell(0,0), new Cell(0,2), new Cell(7,0), new Cell(7,2)], "J"));
        this.rankListCellList.set(Rank.Ten, new RankCellList([new Cell(0,0), new Cell(0,2), new Cell(1,0), new Cell(3,0), new Cell(3,1), new Cell(5,0), new Cell(5,1), new Cell(6,0), new Cell(7,0), new Cell(7,2)], "10"));
        this.rankListCellList.set(Rank.Nine, new RankCellList([new Cell(0,0), new Cell(0,2), new Cell(1,0), new Cell(3,0), new Cell(3,1), new Cell(5,0), new Cell(5,1), new Cell(7,0), new Cell(7,2)], "9"));
        this.rankListCellList.set(Rank.Eight, new RankCellList([new Cell(0,0), new Cell(0,2), new Cell(3,0), new Cell(3,1), new Cell(5,0), new Cell(5,1), new Cell(7,0), new Cell(7,2)], "8"));
        this.rankListCellList.set(Rank.Seven, new RankCellList([new Cell(0,0), new Cell(0,2), new Cell(2,0), new Cell(4,0), new Cell(4,2), new Cell(7,0), new Cell(7,2)], "7"));
        this.rankListCellList.set(Rank.Six, new RankCellList([new Cell(0,0), new Cell(0,2), new Cell(4,0), new Cell(4,2), new Cell(7,0), new Cell(7,2)], "6"));
        this.rankListCellList.set(Rank.Five, new RankCellList([new Cell(0,0), new Cell(0,2), new Cell(4,1), new Cell(7,0), new Cell(7,2)], "5"));
        this.rankListCellList.set(Rank.Four, new RankCellList([new Cell(0,0), new Cell(0,2), new Cell(7,0), new Cell(7,2)], "4"));
        this.rankListCellList.set(Rank.Three, new RankCellList([new Cell(0,1), new Cell(4,1), new Cell(7,1)], "3"));
        this.rankListCellList.set(Rank.Two, new RankCellList([new Cell(0,0), new Cell(7,2)], "2"));
        this.rankListCellList.set(Rank.Ace, new RankCellList([new Cell(0,0), new Cell(0,2), new Cell(7,0), new Cell(7,2)], "A"));
    }

    drawShape(card, point) {
        this.context.save();
        this.context.beginPath();
        this.context.translate(point.x, point.y);
        const rankGrid = this.size.rankSize.getRankGrid();
        const cellList = this.rankListCellList.get(card.rank).cellList;
        const value = this.rankListCellList.get(card.rank).value;
        if (isNaN(value)) {
            this.suitDrawing.draw(card.suit, rankGrid[4][1], 3);
        } else {
            cellList.forEach(cell => {
                this.suitDrawing.draw(card.suit, rankGrid[cell.row][cell.column], 1);
            });
        }

        this.context.restore();       
    }

    drawText(card, point) {
        this.context.save();
        this.context.beginPath();
        this.context.translate(point.x, point.y);
        const suitColorList = this.suitDrawing.getSuitColorList();
        const textGrid = this.size.rankSize.getTextGrid();
        const value = this.rankListCellList.get(card.rank).value;
        this.context.fillStyle = suitColorList.get(card.suit);
        this.context.textAlign = "center";
        this.context.textBaseline = 'middle';
        this.context.font = "bold " + this.size.fontSize + "px sans-serif";
        textGrid.forEach(point => {
            this.context.fillText(value,  point.x, point.y);
        });

        this.context.restore();      
    }
}

export { RankDrawing };
