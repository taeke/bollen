import  { Event } from "../common/event.js";
import { Distance, Point } from "./rectangle.js";

/*
Class for handling mouse action of the user.
*/
class Mouse {
    constructor(canvasElement, header) {
        canvasElement.addEventListener('mousedown', (event) => this.mouseDown(event));
        canvasElement.addEventListener('touchstart', (event) => this.mouseDown(event));
        canvasElement.addEventListener('mousemove', (event) => this.mouseMove(event));
        canvasElement.addEventListener('touchmove', (event) => this.mouseMove(event));     
        canvasElement.addEventListener('mouseup', (event) => this.mouseUp(event));
        canvasElement.addEventListener('touchend', (event) => this.mouseUp(event));
        this.canvasElement = canvasElement;
        this.onDownEvent = new Event();
        this.onMoveEvent = new Event();
        this.onUpEvent = new Event();
        this.header = header;
        this.selectPoint = undefined;
    }

    mouseDown(event) {
        event.preventDefault();
        this.selectPoint = this.getPoint(event);
        this.onDownEvent.trigger(this.selectPoint);
    }

    mouseMove(event) {
        event.preventDefault();
        if(this.selectPoint) {
            this.onMoveEvent.trigger(Distance.create(this.selectPoint, this.getPoint(event)));
        }
    }

    mouseUp(event) {
        this.selectPoint = undefined;
        event.preventDefault();
        this.onUpEvent.trigger();
    }

    getPoint(event) {
        let xCorrection = Math.trunc((window.innerWidth - this.canvasElement.width) / 2);
        if ("ontouchstart" in document.documentElement) {
            return new Point(event.touches[0].clientX -xCorrection, event.touches[0].clientY - this.header.height);
        } else {
            return new Point(event.clientX - xCorrection, event.clientY - this.header.height);
        }
    }
}

export { Mouse };
