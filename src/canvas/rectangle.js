/*
classes for positoning drawing on the classes.
*/

class Distance {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
    
    static create(originalPoint, newPoint) {
        return new Distance(newPoint.x - originalPoint.x, newPoint.y - originalPoint.y);
    }
}

class Point {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    plusDistance(distance) {
        return new Point(distance.x + this.x, distance.y + this.y);
    }
}

class Rectangle {
    constructor(topLeftPoint, bottomRightPoint) {
        this.topLeftPoint = topLeftPoint;
        this.bottomRightPoint = bottomRightPoint;
    }

    isInside(point) {
        let horizontal = this.topLeftPoint.x < point.x && this.bottomRightPoint.x > point.x;
        let vertical = this.topLeftPoint.y < point.y && this.bottomRightPoint.y > point.y;
        return horizontal && vertical;
    }
}

export { Distance, Point, Rectangle };
