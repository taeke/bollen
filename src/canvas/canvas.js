import { Event } from "../common/event.js";
import { Elements } from "../view/elements.js";
import { TableDrawing } from "../draw/tabledrawing.js";
import { Size } from "../size/size.js";
import { Mouse } from "./mouse.js";
import { DragingStates, HandDrawing } from "../draw/handdrawing.js"
import { BidListDrawing } from "../draw/bidlistdrawing.js";
import { TrumpDrawing } from "../draw/trumpdrawing.js";
import { Distance } from "./rectangle.js";
import { AskedDrawing } from "../draw/askeddrawing.js";

/*
Class for drawing the playing cards and communicating back to server cards played.
*/
class Canvas extends Elements {
    constructor(contentElement) {
        super();
        this.canvasElement = undefined;
        this.canvasContext = undefined;
        
        this.contentElement = contentElement;

        this.onPlayedCard = new Event();
        this.onPlaceBid = new Event();

        this.tableDrawing = undefined;
        this.handDrawing = undefined;
        this.bidListDrawing = undefined;
        this.trumpDrawing = undefined;
        this.askedDrawing = undefined;
        this.size = undefined;
        this.mouse = undefined;
        this.playerstate =undefined;
        this.draggingState = DragingStates.NotYourTurn;
    }
    
    draw(playerstate) {
        this.handDrawing.selectedCard = undefined;
        this.tableDrawing.droppedCard = undefined;
        this.playerstate = playerstate;
        if(this.playerstate.itsYourTurnToPlayACard) this.draggingState = DragingStates.NotSelected;
        this.internalDraw();
    }

    internalDraw() {
        this.canvasContext.clearRect(0,0, this.canvasElement.width, this.canvasElement.height);
        this.trumpDrawing.draw(this.playerstate.trump);
        this.askedDrawing.draw(this.playerstate.askedSuit);
        this.bidListDrawing.draw(this.playerstate.itsYourTurnToPlaceBid, this.playerstate.validBids);
        this.tableDrawing.draw(this.playerstate.playerList, this.draggingState);
        this.handDrawing.draw(this.playerstate.cardsInHand, this.draggingState);
    }

    create(header) {
        this.canvasElement = this.createCanvasElement(header);
        this.size = new Size(this.canvasElement);
        this.size.calculate();
        this.canvasContext = this.canvasElement.getContext('2d');
        this.contentElement.appendChild(this.canvasElement);
        this.tableDrawing = new TableDrawing(this.canvasContext, this.size);
        this.handDrawing = new HandDrawing(this.canvasContext, this.size);
        this.bidListDrawing = new BidListDrawing(this.canvasContext, this.size);
        this.trumpDrawing = new TrumpDrawing(this.canvasContext, this.size);
        this.askedDrawing = new AskedDrawing(this.canvasContext, this.size);
        this.mouse = new Mouse(this.canvasElement, header);
        this.mouse.onDownEvent.addListener((point) => this.handleMouseDown(point));
        this.mouse.onMoveEvent.addListener((distance) => this.handleMouseMove(distance));
        this.mouse.onUpEvent.addListener(() => this.handleMouseUp());
    }

    handleMouseDown(point) {
        if(this.playerstate.itsYourTurnToPlaceBid) {
            const bid = this.bidListDrawing.selectBid(point);
            if(bid != undefined) {
                this.onPlaceBid.trigger(bid);
            }
        }

        if(this.draggingState == DragingStates.NotSelected) {
            this.handDrawing.selectCard(point);
            if(this.handDrawing.selectedCard) this.draggingState = DragingStates.Selected;
        }

        this.internalDraw();
    }

    handleMouseMove(distance) {
        if(this.draggingState == DragingStates.Selected) {
            this.handDrawing.draggingDistance = distance;
            const newPoint = this.handDrawing.getNewPoint();
            if (this.tableDrawing.canDrop(this.playerstate.playerList, newPoint)) {
                this.draggingState = DragingStates.Dropped;
                this.tableDrawing.droppedCard = this.handDrawing.selectedCard;
                this.onPlayedCard.trigger(this.tableDrawing.droppedCard);
            }

            this.internalDraw();
        }
    }

    handleMouseUp() {
        if(this.draggingState == DragingStates.Selected || this.draggingState == DragingStates.NotSelected) {
            this.draggingState = DragingStates.NotSelected;
        }
        
        this.handDrawing.selectedCard = undefined;
        this.handDrawing.draggingDistance = new Distance(0, 0);;
        this.internalDraw();
    }
}

export { Canvas };
