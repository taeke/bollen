import { Canvas } from "./canvas.js";

const canvas = new Canvas();

import { Mouse } from "./mouse.js";

const mouse = new Mouse();

import { Distance, Point, Rectangle } from "./rectangle.js";

const distance = new Distance();

const point = new Point();

const rectangle = new Rectangle();

