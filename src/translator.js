class Translator {
    translations = {
        join : {
            en: "join",
            nl: "doe mee"
        },
        showServerNotAvailable: {
            en: "The server can't be found. Refresh once again or ask for a new link.",
            nl: "De server kan niet gevonden worden. Ververs de pagina nogmaals of vraag om een nieuwe link." 
        },
        tooMany: {
            en: "I am sorry. Too many players already joined this game.",
            nl: "Het spijt me. Te veel spelers doen al mee aan dit spel."
        },
        copyLink: {
            en: "Copy the inventation.",
            nl: "Kopieer de uitnodiging."
        },
        yourName: {
            en: "Your name",
            nl: "Je naam"
        },
        nameConstrain: {
            en: "Name can only contain letters and a maximum of 8 characters and a minimum of 2 characters.",
            nl: "De naam kan alleen letters bevatten en maximaal 8 karakters en minimaal 2 karakters."
        },
        newGame: {
            en: "New game",
            nl: "Nieuw spel"
        },
        copiedLink: {
            en: "Copied link to clipboard",
            nl: "De link is gekopieerd naar het klembord."
        },
        gameStarted: {
            en: "Game started",
            nl: "Het spel is gestart"
        },
        somethingWrong: {
            en: "Something went wrong. I am sorry.",
            nl: "Er is iets mis gegaan. Het spijt me."
        },
        master: {
            en: "Bollen game master",
            nl: "Bollen spelleider"
        },
        lostConnection: {
            en: "Lost connection with the game master",
            nl: "Geen verbinding meer met de spelleider"
        },
        trump: {
            en: "Trump :",
            nl: "Troef :"
        },
        asked: {
            en: "Asked :",
            nl: "Gevraagd :"
        }, 
        previous : {
            en: "Previous",
            nl: "Vorige"
        }, 
        turn : {
            en: "It's %%'s turn.",
            nl: "%% is aan de beurt."            
        }
    }

    statusses = {
        "Waiting" : {
            en: "Waiting for other players",
            nl: "Wachten op andere spelers"
        },
        "Joining": {
            en: "Joining",
            nl: "Verbinden"
        },
        "GameStarted": {
            en: "Game started",
            nl: "Het spel is gestart"
        },
        "CantJoin": {
            en: "Can't join.",
            nl: "Ik kan geen verbinding maken."
        },
        "Error": {
            en: "Error",
            nl: "Fout"
        },
        "Disconnected": {
            en: "disconnected",
            nl: "niet verbonden"
        },
    }

    constructor(langugage) {
        this.langugage = langugage;
    }

    translate(string) {
        return this.translations[string][this.langugage];
    }

    fromStatus(status) {
        return this.statusses[status.name][this.langugage];
    }
}

export { Translator };
