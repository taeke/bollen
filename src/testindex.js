import Round from "./server/round.js";
import State from "./server/state.js";
import { Player, Status, PlayerState }  from "./common/playerstate.js";
import { Card, Deck, Rank, Suit }  from "./common/deck.js";
import Header from "./view/header.js";
import Canvas from "./canvas/canvas.js";
import Elements from "./view/elements.js";
import Size from "./canvas/size.js";
import SuitDrawing from "./canvas/suitdrawing.js";
import RankDrawing from "./canvas/rankdrawing.js";
import { Point } from "./canvas/rectangle.js";
import CardDrawing from "./canvas/carddrawing.js";
import { BidListDrawing } from "./canvas/bidlistdrawing.js";
import TrumpDrawing from "./canvas/trumpdrawing.js";

// test Round.createRoundList
// console.log(Round.createRoundList(3));

// test Deck.shuffle
// console.log(Deck.shuffle(Rank.Seven));

// test State.dealCards
// const state = new State();
// state.addPlayer(new Player(1, "Taeke", true));
// state.addPlayer(new Player(2, "Eva", true));
// state.addPlayer(new Player(3, "Lisan", true));
// state.startGame();
// state.currentRoundIndex = 4;
// state.dealCards();
// console.log(state);

// test PlayerState.setYourTurnToPlaceBid
// const state = new State();
// state.addPlayer(new Player(1, "Taeke", true));
// state.addPlayer(new Player(2, "Eva", true));
// state.addPlayer(new Player(3, "Lisan", true));
// state.getPlayerState(1).setYourTurnToPlaceBid(1);
// state.getPlayerState(2).setYourTurnToPlaceBid(2);
// console.log(state);

// test PlayerState.setYourTurnToPlayCard
// const state = new State();
// state.addPlayer(new Player(1, "Taeke", true));
// state.addPlayer(new Player(2, "Eva", true));
// state.addPlayer(new Player(3, "Lisan", true));
// state.getPlayerState(1).setYourTurnToPlayCard(1);
// state.getPlayerState(2).setYourTurnToPlayCard(2);
// console.log(state);

//test PlayerState.switchField
// const player2 = new Player(2, "Eva", true);
// player2.itsYourTurnToPlayACard = true;
// const playerState = PlayerState.makeUnique(new Player(1, "Taeke", true),[player2, new Player(3, "Lisan", true)], Status.Waiting);
// playerState.switchField(1, "itsYourTurnToPlayACard");
// console.log(playerState);

// test PlayerState.setField
// const player2 = new Player(2, "Eva", false);
// const playerState = PlayerState.makeUnique(new Player(1, "Taeke", true),[player2, new Player(3, "Lisan", true)], Status.Waiting);
// playerState.setField(1, "isConnected", false);
// console.log(playerState);

// test PlayerState.setIsConnected
// const player2 = new Player(2, "Eva", false);
// const playerState = PlayerState.makeUnique(new Player(1, "Taeke", true),[player2, new Player(3, "Lisan", true)], Status.Waiting);
// playerState.setIsConnected(1, false);
// console.log(playerState);

// test State.connectionLost
// const state = new State();
// state.addPlayer(new Player(1, "Taeke", true));
// state.addPlayer(new Player(2, "Eva", true));
// state.addPlayer(new Player(3, "Lisan", true));
// state.connectionLost(2);
// console.log(state);

// test State.reconnectingPlayer
// const state = new State();
// state.addPlayer(new Player(1, "Taeke", true));
// state.addPlayer(new Player(2, "Eva", true));
// state.addPlayer(new Player(3, "Lisan", true));
// state.connectionLost(2);
// state.reconnectingPlayer(2);
// console.log(state);

// test State.resetConnected
// const state = new State();
// state.addPlayer(new Player(1, "Taeke", true));
// state.addPlayer(new Player(2, "Eva", true));
// state.addPlayer(new Player(3, "Lisan", true));
// state.resetConnected();
// console.log(state);

// test PlayerState.setCard
// const state = new State();
// state.addPlayer(new Player(1, "Taeke", true));
// state.addPlayer(new Player(2, "Eva", true));
// state.addPlayer(new Player(3, "Lisan", true));
// const playerState = state.getPlayerState(1);
// playerState.cardsInHand.push(new Card(Rank.Eight, Suit.Diamonds));
// playerState.setCard(1, new Card(Rank.Eight, Suit.Diamonds));
// console.log(playerState);

// test areAllCardsOfTrickPlayed
// const state = new State();
// state.addPlayer(new Player(1, "Taeke", true));
// state.addPlayer(new Player(2, "Eva", true));
// state.addPlayer(new Player(3, "Lisan", true));
// const cardList = [new Card(Rank.Jack, Suit.Diamonds), new Card(Rank.Ten, Suit.Hearts), new Card(Rank.Nine, Suit.Hearts)];
// for (let index = 0; index < state.playerStateList.length; index++) {
//     const playerState = state.playerStateList[index];
//     playerState.cardsInHand.push(cardList[index]);
// }
// console.log(state.areAllCardsOfTrickPlayed());
// for (let index = 0; index < state.playerStateList.length; index++) {
//     const playerState = state.playerStateList[index];
//     playerState.setCard(playerState.clientId, cardList[index]);
//     console.log(state.areAllCardsOfTrickPlayed());
// }

// test areAllCardsOfTrickPlayed
// const state = new State();
// state.addPlayer(new Player(1, "Taeke", true));
// state.addPlayer(new Player(2, "Eva", true));
// state.addPlayer(new Player(3, "Lisan", true));
// const cardList = [new Card(Rank.Jack, Suit.Diamonds), new Card(Rank.Ten, Suit.Hearts), new Card(Rank.Nine, Suit.Hearts)];
// for (let index = 0; index < state.playerStateList.length; index++) {
//     const playerState = state.playerStateList[index];
//     playerState.cardsInHand.push(cardList[index]);
// }
// console.log(state.areAlleCardsOfRoundPlayed());
// for (let index = 0; index < state.playerStateList.length; index++) {
//     const playerState = state.playerStateList[index];
//     playerState.setCard(playerState.clientId, cardList[index]);
//     console.log(state.areAlleCardsOfRoundPlayed());
// }
// test PlayerState.setBid
// const player2 = new Player(2, "Eva", false);
// const playerState = PlayerState.makeUnique(new Player(1, "Taeke", true),[player2, new Player(3, "Lisan", true)], Status.Waiting);
// playerState.setBid(1, 4);
// console.log(playerState);

// test resetCardsOnTable
// const state = new State();
// state.addPlayer(new Player(1, "Taeke", true));
// state.addPlayer(new Player(2, "Eva", true));
// state.addPlayer(new Player(3, "Lisan", true));
// const cardList = [new Card(Rank.Jack, Suit.Diamonds), new Card(Rank.Ten, Suit.Hearts), new Card(Rank.Nine, Suit.Hearts)];
// for (let index = 0; index < state.playerStateList.length; index++) {
//     const playerState = state.playerStateList[index];
//     playerState.cardsInHand.push(cardList[index]);
// }
// for (let index = 0; index < state.playerStateList.length; index++) {
//     const playerState = state.playerStateList[index];
//     playerState.setCard(playerState.clientId, cardList[index]);
// }
// state.resetCardsOnTable();
// console.log(state);

// test isGameFinished
// const state = new State();
// state.addPlayer(new Player(1, "Taeke", true));
// state.addPlayer(new Player(2, "Eva", true));
// state.addPlayer(new Player(3, "Lisan", true));
// state.startGame();
// state.currentRoundIndex = 11;
// console.log(state.isGameFinished());

// test Card.getWinnerIndex
// const cards1 = [new Card(Rank.Ace, Suit.Clubs), new Card(Rank.Eight, Suit.Clubs), new Card(Rank.King, Suit.Hearts), new Card(Rank.Jack, Suit.Hearts)];
// console.log("expect 2 actual : " + Card.getWinnerIndex(cards1, Suit.Hearts, Suit.Clubs));
// const cards2 = [new Card(Rank.Ace, Suit.Clubs), new Card(Rank.Eight, Suit.Clubs), new Card(Rank.King, Suit.Hearts), new Card(Rank.Jack, Suit.Hearts)];
// console.log("expect 0 actual : " + Card.getWinnerIndex(cards2, Suit.Diamonds, Suit.Clubs));
// const cards3 = [new Card(Rank.Seven, Suit.Clubs), new Card(Rank.Eight, Suit.Clubs), new Card(Rank.Nine, Suit.Clubs), new Card(Rank.Ten, Suit.Clubs)];
// console.log("expect 3 actual : " + Card.getWinnerIndex(cards3, Suit.Diamonds, Suit.Clubs));

// test getWinnerIndex
// const state = new State();
// state.addPlayer(new Player(1, "Taeke", true));
// state.addPlayer(new Player(2, "Eva", true));
// state.addPlayer(new Player(3, "Lisan", true));
// state.trump = Suit.Hearts;
// const cardList = [new Card(Rank.Ace, Suit.Clubs), new Card(Rank.Eight, Suit.Clubs), new Card(Rank.King, Suit.Hearts)];
// for (let index = 0; index < state.playerStateList.length; index++) {
//     const playerState = state.playerStateList[index];
//     playerState.cardsInHand.push(cardList[index]);
// }
// for (let index = 0; index < state.playerStateList.length; index++) {
//     const playerState = state.playerStateList[index];
//     playerState.setCard(playerState.clientId, cardList[index]);
// }
// console.log(state.getWinnerIndex());

// test nextIndex
// const state = new State();
// state.addPlayer(new Player(1, "Taeke", true));
// state.addPlayer(new Player(2, "Eva", true));
// state.addPlayer(new Player(3, "Lisan", true));
// state.currentPlayerIndex = state.getNextIndex();
// console.log("expect 1 got : " + state.currentPlayerIndex);
// state.currentPlayerIndex = state.getNextIndex();
// console.log("expect 2 got : " + state.currentPlayerIndex);
// state.currentPlayerIndex = state.getNextIndex();
// console.log("expect 0 got : " + state.currentPlayerIndex);

// test setNextToPlayCard with not all cards played
// const state = new State();
// state.addPlayer(new Player(1, "Taeke", true));
// state.addPlayer(new Player(2, "Eva", true));
// state.addPlayer(new Player(3, "Lisan", true));
// state.setNextToPlayCard();
// console.log("expect 1 got : " + state.currentPlayerIndex);
// console.log("expect 1 got : " + state.startPlayerIndex);
// console.log(state);

// test setNextToPlayCard with all cards played
// const state = new State();
// state.addPlayer(new Player(1, "Taeke", true));
// state.addPlayer(new Player(2, "Eva", true));
// state.addPlayer(new Player(3, "Lisan", true));
// state.trump = Suit.Hearts;
// const cardList = [new Card(Rank.Ace, Suit.Clubs), new Card(Rank.Eight, Suit.Clubs), new Card(Rank.King, Suit.Hearts)];
// for (let index = 0; index < state.playerStateList.length; index++) {
//     const playerState = state.playerStateList[index];
//     playerState.cardsInHand.push(cardList[index]);
// }
// for (let index = 0; index < state.playerStateList.length; index++) {
//     const playerState = state.playerStateList[index];
//     playerState.setCard(playerState.clientId, cardList[index]);
// }
// state.setNextToPlayCard();
// console.log("expect 2 got : " + state.currentPlayerIndex);
// console.log("expect 2 got : " + state.startPlayerIndex);
// console.log(state);

// test setNextPlayer with all cards played
// const state = new State();
// state.addPlayer(new Player(1, "Taeke", true));
// state.addPlayer(new Player(2, "Eva", true));
// state.addPlayer(new Player(3, "Lisan", true));
// state.trump = Suit.Hearts;
// const cardList = [new Card(Rank.Ace, Suit.Clubs), new Card(Rank.Eight, Suit.Clubs), new Card(Rank.King, Suit.Hearts)];
// for (let index = 0; index < state.playerStateList.length; index++) {
//     const playerState = state.playerStateList[index];
//     playerState.cardsInHand.push(cardList[index]);
// }
// for (let index = 0; index < state.playerStateList.length; index++) {
//     const playerState = state.playerStateList[index];
//     playerState.setCard(playerState.clientId, cardList[index]);
// }
// state.setNextPlayer();
// console.log("expect 0 got : " + state.currentRoundIndex);

// test State.setNextRound
// const state = new State();
// state.addPlayer(new Player(1, "Taeke", true));
// state.addPlayer(new Player(2, "Eva", true));
// state.addPlayer(new Player(3, "Lisan", true));
// state.startGame();
// state.currentRoundIndex = 1;
// state.setNextRound();
// console.log(state);

//test State.resetBidsCurrentRound
// const state = new State();
// state.addPlayer(new Player(1, "Taeke", true));
// state.addPlayer(new Player(2, "Eva", true));
// state.addPlayer(new Player(3, "Lisan", true));
// state.startGame();
// state.playerStateList.forEach(playerState => playerState.setBid(playerState.clientId, 1));
// state.resetBidsCurrentRound();
// console.log(state);

//test State.areAllBidsOfRoundPlaced
// const state = new State();
// state.addPlayer(new Player(1, "Taeke", true));
// state.addPlayer(new Player(2, "Eva", true));
// state.addPlayer(new Player(3, "Lisan", true));
// state.startGame();
// console.log(state.areAllBidsOfRoundPlaced());
// state.playerStateList.forEach(playerState => playerState.setBid(playerState.clientId, 1));
// console.log(state.areAllBidsOfRoundPlaced());

//test State.playCard and State.setBid
// const state = new State();
// state.addPlayer(new Player(1, "Taeke", true));
// state.addPlayer(new Player(2, "Eva", true));
// state.addPlayer(new Player(3, "Lisan", true));
// state.startGame();
// state.setBid(1, 1);
// state.setBid(2, 0);
// state.setBid(3, 1);
// state.playCard(1, state.getPlayerState(1).cardsInHand[0]);
// console.log(state);

// test json
// const state = new State();
// state.addPlayer(new Player(1, "Taeke", true));
// state.addPlayer(new Player(2, "Eva", true));
// state.addPlayer(new Player(3, "Lisan", true));
// state.startGame();
// state.setBid(1, 1);
// state.setBid(2, 0);
// state.setBid(3, 1);
// state.playCard(1, state.getPlayerState(1).cardsInHand[0]);
// state.playCard(2, state.getPlayerState(2).cardsInHand[0]);
// state.playCard(3, state.getPlayerState(3).cardsInHand[0]);
// state.setBid(1, 1);
// state.setBid(2, 0);
// state.setBid(3, 1);
// state.playCard(1, state.getPlayerState(1).cardsInHand[0]);
// const jsonString = JSON.stringify(state.playerStateList[0]);
// const newPlayerState = PlayerState.fromJson(JSON.parse(jsonString));
// console.log(newPlayerState);

// test HandSize.getSlots
// const elements = new Elements();
// const containerElement = elements.createDivElement("container");
// const header = new Header(containerElement, false);
// header.show("TEST");
// const contentElement = elements.createDivElement("content");
// containerElement.appendChild(contentElement);
// const canvas = new Canvas(contentElement);
// canvas.create(header);
// const size = new Size(canvas.canvasElement);
// size.calculate();
// console.log(size.handSize.getSlots(4));

// test TableSize.getSlots
// const elements = new Elements();
// const containerElement = elements.createDivElement("container");
// const header = new Header(containerElement, false);
// header.show("TEST");
// const contentElement = elements.createDivElement("content");
// containerElement.appendChild(contentElement);
// const canvas = new Canvas(contentElement);
// canvas.create(header);
// const size = new Size(canvas.canvasElement);
// size.calculate();
// console.log(size.tableSize.getSlots(4));

// test canvas
// const state = new State();
// state.addPlayer(new Player(1, "Taeke", true));
// state.addPlayer(new Player(2, "Eva", true));
// state.addPlayer(new Player(3, "Lisan", true));
// state.startGame();
// state.setBid(1, 1);
// state.setBid(2, 0);
// state.setBid(3, 1);
// state.playCard(1, state.getPlayerState(1).cardsInHand[0]);
// state.playCard(2, state.getPlayerState(2).cardsInHand[0]);
// state.playCard(3, state.getPlayerState(3).cardsInHand[0]);
// state.setBid(1, 1);
// state.setBid(2, 0);
// state.setBid(3, 1);
// state.playCard(1, state.getPlayerState(1).cardsInHand[0]);
// state.playCard(2, state.getPlayerState(2).cardsInHand[0]);
// state.playCard(3, state.getPlayerState(3).cardsInHand[0]);
// state.playCard(1, state.getPlayerState(1).cardsInHand[0]);
// state.playCard(2, state.getPlayerState(2).cardsInHand[0]);
// state.playCard(3, state.getPlayerState(3).cardsInHand[0]);
// state.setBid(1, 1);
// state.setBid(2, 0);
// state.setBid(3, 1);
// state.playCard(1, state.getPlayerState(1).cardsInHand[0]);
// state.playCard(2, state.getPlayerState(2).cardsInHand[0]);
// state.playCard(3, state.getPlayerState(3).cardsInHand[0]);
// state.playCard(1, state.getPlayerState(1).cardsInHand[0]);
// state.playCard(2, state.getPlayerState(2).cardsInHand[0]);
// state.playCard(3, state.getPlayerState(3).cardsInHand[0]);
// state.playCard(1, state.getPlayerState(1).cardsInHand[0]);
// state.playCard(2, state.getPlayerState(2).cardsInHand[0]);
// state.playCard(3, state.getPlayerState(3).cardsInHand[0]);
// state.setBid(1, 1);
// state.setBid(2, 0);
// state.setBid(3, 1);
// state.playCard(1, state.getPlayerState(1).cardsInHand[0]);
// state.playCard(2, state.getPlayerState(2).cardsInHand[0]);
// state.playCard(3, state.getPlayerState(3).cardsInHand[0]);
// state.playCard(1, state.getPlayerState(1).cardsInHand[0]);
// state.playCard(2, state.getPlayerState(2).cardsInHand[0]);
// state.playCard(3, state.getPlayerState(3).cardsInHand[0]);
// state.playCard(1, state.getPlayerState(1).cardsInHand[0]);
// state.playCard(2, state.getPlayerState(2).cardsInHand[0]);
// state.playCard(3, state.getPlayerState(3).cardsInHand[0]);
// state.playCard(1, state.getPlayerState(1).cardsInHand[0]);
// state.playCard(2, state.getPlayerState(2).cardsInHand[0]);
// state.playCard(3, state.getPlayerState(3).cardsInHand[0]); 
// state.setBid(1, 1);
// state.setBid(2, 0);
// state.setBid(3, 1);
// state.playCard(1, state.getPlayerState(1).cardsInHand[0]);
// const elements = new Elements();
// const containerElement = elements.createDivElement("container");
// const header = new Header(containerElement, false);
// header.show("TEST");
// const contentElement = elements.createDivElement("content");
// containerElement.appendChild(contentElement);
// const canvas = new Canvas(contentElement);
// canvas.create(header);
// canvas.draw(state.getPlayerState(3));

// test suitdrawing;
// const elements = new Elements();
// const containerElement = elements.createDivElement("container");
// const header = new Header(containerElement, false);
// header.show("TEST");
// const contentElement = elements.createDivElement("content");
// containerElement.appendChild(contentElement);
// const canvas = new Canvas(contentElement);
// canvas.create(header);
// const size = new Size(canvas.canvasElement);
// size.calculate();
// const suitDrawing = new SuitDrawing(canvas.canvasContext, size);
// suitDrawing.draw(Suit.Hearts, new Point(100, 100));

// test rankdrawing;
// const elements = new Elements();
// const containerElement = elements.createDivElement("container");
// const header = new Header(containerElement, false);
// header.show("TEST");
// const contentElement = elements.createDivElement("content");
// containerElement.appendChild(contentElement);
// const canvas = new Canvas(contentElement);
// canvas.create(header);
// const size = new Size(canvas.canvasElement);
// size.calculate();
// const rankDrawing = new RankDrawing(canvas.canvasContext, size);
// const card = new Card(Rank.Five, Suit.Hearts)
// rankDrawing.draw(card, new Point(100, 100));

// test carddrawing;
// const elements = new Elements();
// const containerElement = elements.createDivElement("container");
// const header = new Header(containerElement, false);
// header.show("TEST");
// const contentElement = elements.createDivElement("content");
// containerElement.appendChild(contentElement);
// const canvas = new Canvas(contentElement);
// canvas.create(header);
// const size = new Size(canvas.canvasElement);
// size.calculate();
// const cardDrawing = new CardDrawing(canvas.canvasContext, size);
// const card = new Card(Rank.Five, Suit.Hearts)
// cardDrawing.draw(new Point(100, 100), card);
// const trumpDrawing = new TrumpDrawing(canvas.canvasContext, size);
// trumpDrawing.draw(Suit.Spades);


// test carddrawing;
// const elements = new Elements();
// const containerElement = elements.createDivElement("container");
// const header = new Header(containerElement, false);
// header.show("TEST");
// const contentElement = elements.createDivElement("content");
// containerElement.appendChild(contentElement);
// const canvas = new Canvas(contentElement);
// canvas.create(header);
// const size = new Size(canvas.canvasElement);
// size.calculate();
// const bidListDrawing = new BidListDrawing(canvas.canvasContext, size);
// bidListDrawing.draw(true, ["0", "1"]);


const elements = new Elements();
const containerElement = elements.createDivElement("container");
const header = new Header(containerElement, false);
header.show("TEST");
const contentElement = elements.createDivElement("content");
containerElement.appendChild(contentElement);
const canvas = new Canvas(contentElement);
canvas.create(header);
const size = new Size(canvas.canvasElement);
size.calculate();

const canvasContext = canvas.canvasElement.getContext('2d');


// canvasContext.beginPath();
// canvasContext.rect(20, 20, 150, 100);
// canvasContext.fillStyle = "red";
// canvasContext.fill();

// canvasContext.beginPath();
// canvasContext.rect(40, 40, 150, 100);
// canvasContext.fillStyle = "blue";
// canvasContext.fill();
//canvasContext.save();
//canvasContext.lineWidth = 2;
//canvasContext.strokeStyle = "black"; 
// canvasContext.beginPath();
// canvasContext.fillStyle = "blue";
// canvasContext.translate(100, 100);
// canvasContext.moveTo(40, 0);
// canvasContext.arc(0, 0, 40, 0, 2 * Math.PI);
// canvasContext.stroke();  
// canvasContext.moveTo(0,0);
// canvasContext.lineTo(200, 0);
// canvasContext.lineTo(200, 300);
// canvasContext.lineTo(0, 300);
// canvasContext.lineTo(0, 0);
// canvasContext.fill();
// //canvasContext.restore();

// //canvasContext.save();
// canvasContext.beginPath();
// canvasContext.fillStyle = "red";
// canvasContext.translate(200, 200);
// canvasContext.moveTo(0,0);
// canvasContext.lineTo(200, 0);
// canvasContext.lineTo(200, 300);
// canvasContext.lineTo(0, 300);
// canvasContext.lineTo(0, 0);
// canvasContext.fill();
//canvasContext.restore();       