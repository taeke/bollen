import { BollenConfig } from "./bollenconfig.js";
import { Start } from "./start.js";

globalThis.bollenConfig = BollenConfig.create();

const start = new Start()
start.run();
