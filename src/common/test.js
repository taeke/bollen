import { DataType, Data } from "./data.js"

const dataType = new DataType();

const data = new Data();

import { Suit, Rank, Card, Deck } from "./deck.js";

const suit = new Suit();

const rank = new Rank();

console.log("getRankForNumberOfCards");
console.log(Rank.getRankForNumberOfCards(4));

const card = new Card();

console.log("Expect 1");
console.log(Card.getWinnerIndex([new Card(Rank.Two, Suit.Clubs), new Card(Rank.Three, Suit.Clubs)], Suit.Diamonds, Suit.Clubs));
console.log("Expect 0");
console.log(Card.getWinnerIndex([new Card(Rank.Three, Suit.Clubs), new Card(Rank.Two, Suit.Clubs)], Suit.Diamonds, Suit.Clubs));
console.log("Expect 0");
console.log(Card.getWinnerIndex([new Card(Rank.Two, Suit.Clubs), new Card(Rank.Three, Suit.Hearts)], Suit.Diamonds, Suit.Clubs));
console.log("Expect 1");
console.log(Card.getWinnerIndex([new Card(Rank.Three, Suit.Clubs), new Card(Rank.Two, Suit.Diamonds)], Suit.Diamonds, Suit.Clubs));
console.log("Expect 1");
console.log(Card.getWinnerIndex([new Card(Rank.Two, Suit.Clubs), new Card(Rank.Three, Suit.Clubs)], Suit.Clubs, Suit.Clubs));
console.log("Expect 1");
console.log(Card.getWinnerIndex([new Card(Rank.Queen, Suit.Clubs), new Card(Rank.King, Suit.Clubs)], Suit.Hearts, Suit.Clubs));
console.log("Expect 1");
console.log(Card.getWinnerIndex([new Card(Rank.Ace, Suit.Spades), new Card(Rank.Ten, Suit.Clubs)], Suit.Hearts, Suit.Clubs));
console.log("Expect 1");
console.log(Card.getWinnerIndex([new Card(Rank.Queen, Suit.Diamonds), new Card(Rank.Queen, Suit.Clubs)], Suit.Spades, Suit.Clubs));

const deck = new Deck();

import { Enum } from "./enum.js";

import { Event } from "./event.js";

const event = new Event();

import { Player, PlayerState, Status } from "./playerstate.js";

const player = new Player();

const playerState = new PlayerState();

const status = new Status();

