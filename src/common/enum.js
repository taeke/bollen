class Enum {
    static fromJson(Type, json) {
        return Object.values(Type).find(type => type.name == json.name);
    }
}

export { Enum };
