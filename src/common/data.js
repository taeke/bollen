/*
Classes for container / wrapper around data send between client and server.
*/
class DataType {
    static TooMany = new DataType("TooMany");
    static PlayerState = new DataType("PlayerState");
    static Ping = new DataType("Ping");
    static Pong = new DataType("Pong");
    static PlayCard = new DataType("PlayCard");
    static PlaceBid = new DataType("PlaceBid")

    constructor(name) {
        this.name = name;
    }
}

class Data {
    constructor(dataType, client, content) {
        this.dataType = dataType;
        this.client = client;
        this.content = content;
    }
}

export { DataType, Data };
