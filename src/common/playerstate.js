import { Enum } from "./enum.js";
import { Card, Rank, Suit } from "./deck.js";

/*
Classes to hold the current state of each player.
*/

class Status {
    static Joining = new Status("Joining");
    static Waiting = new Status("Waiting");
    static GameStarted = new Status("GameStarted");
    static CantJoin = new Status("CantJoin");
    static Error = new Status("Error");
    static Disconnected = new Status("Disconnected");

    constructor(name) {
        this.name = name;
    }
}

class Player {
    constructor(clientId, name, isConnected) {
        this.clientId = clientId;
        this.name = name;
        this.isConnected = isConnected;
        this.bidCurrentRound = undefined;
        this.scoreCurrentRound = undefined;
        this.numberOfBollen = 0;
        this.cardOnTable = undefined;
        this.itsYourTurnToPlayACard = false;
        this.itsYourTurnToPlaceBid = false;
        this.previousCard = undefined;
    }

    static fromJson(json) {
        const player = Object.assign(new Player(), json);
        if(player.cardOnTable) player.cardOnTable = Card.fromJson(player.cardOnTable);
        if(player.previousCard) player.previousCard = Card.fromJson(player.previousCard);
        return player;
    }
}

class PlayerState {
    constructor(clientId, name, isConnected, status, playerList) {
        this.version = 1;
        this.status = status;
        this.trump = undefined;
        this.askedSuit = undefined;
        this.playerList = playerList;

        this.clientId = clientId;
        this.name = name;
        this.isConnected = isConnected;
        this.bidCurrentRound = undefined;
        this.scoreCurrentRound = 0;
        this.numberOfBollen = 0;
        this.cardOnTable = undefined;
        this.itsYourTurnToPlaceBid = false;
        this.itsYourTurnToPlayACard = false;

        this.cardsInHand = [];
        this.validBids = [];
    }

    updateVersion() {
        this.version++;
    }

    setYourTurnToPlaceBid(clientId) {
        this.switchField(clientId, "itsYourTurnToPlaceBid");
        if(clientId == this.clientId) {
            this.setValidBids();
        }
    }

    addBol(clientId, newBollen) {
        this.playerList.forEach(player => {
            if(player.clientId == clientId) player.numberOfBollen = newBollen;
        });
    }

    addScore(clientId, newScore) {
        this.playerList.forEach(player => {
            if(player.clientId == clientId) {
                player.scoreCurrentRound = newScore;
            }
        });          
    }

    resetScore() {
        this.scoreCurrentRound = 0;
        this.playerList.forEach(player => {
            player.scoreCurrentRound  = 0;
        });        
    }

    setValidBids() {
        this.validBids = [];
        const isLastToBid = this.playerList.filter(player => player.bidCurrentRound == undefined).length == 1;
        let totalBids = 0;
        this.playerList.forEach(player => {
            if(player.bidCurrentRound) totalBids = totalBids + player.bidCurrentRound;
        });

        for (let index = 0; index <= this.cardsInHand.length; index++) {
            if(isLastToBid && totalBids + index == this.cardsInHand.length) continue;
            this.validBids.push(index);
        }
    }

    setYourTurnToPlayCard(clientId) {
        this.switchField(clientId, "itsYourTurnToPlayACard");
    }

    switchField(clientId, fieldName) {
        this[fieldName] = this.clientId == clientId;
        this.playerList.forEach(player => {
            player[fieldName] = player.clientId == clientId;
        });        
    }

    setField(clientId, fieldName, value) {
        if(this.clientId == clientId) this[fieldName] = value;
        this.playerList.forEach(player => {
            if(player.clientId == clientId) player[fieldName] = value;
        });                
    }

    setIsConnected(clientId, isConnected) {
       this.setField(clientId, "isConnected", isConnected);
    }

    resetCard() {
        this.cardOnTable = undefined;
        this.playerList.forEach(player => {
            if(player.cardOnTable) {
                player.previousCard = player.cardOnTable;
                player.cardOnTable = undefined;
            }
        });
    }

    setCard(clientId, cardToPlay) {
        if(this.clientId == clientId) {
            this.cardOnTable = cardToPlay;
            if(cardToPlay) {
                this.cardsInHand.splice(this.cardsInHand.findIndex(card => card.suit == cardToPlay.suit && card.rank == cardToPlay.rank), 1);
            }
        }

        this.setAskedSuit(cardToPlay);
        this.setField(clientId, "cardOnTable", cardToPlay);
        this.setField(clientId, "itsYourTurnToPlayACard", false);
    }

    setAskedSuit(card) {
        if(card == undefined || this.askedSuit) return;
        this.askedSuit = card.suit;
    }

    setBid(clientId, bid) {
        this.setField(clientId, "bidCurrentRound", bid);
        this.setField(clientId, "itsYourTurnToPlaceBid", false);
    }

    getCardsOnTable() {
        return this.playerList.map(player => player.cardOnTable).filter(card => card != undefined);
    }

    static makeUnique(player, playerList, status) {
        const uniqueName = PlayerState.makeNameUnique(playerList, player.name);
        playerList.push(new Player(player.clientId, uniqueName, player.isConnected));
        return new PlayerState(player.clientId, uniqueName, player.isConnected, status, playerList);
    }

    static makeNameUnique(playerList, name) {
        const existingPlayer = playerList.find((player) => player.name == name);
        if(existingPlayer) {
            const newName = name + "1";
            return PlayerState.makeNameUnique(playerList, newName);
        }

        return name;        
    }

    static fromJson(json) {
        const playerState = Object.assign(new PlayerState(), json);
        playerState.status = Enum.fromJson(Status, playerState.status);
        if(playerState.cardOnTable) playerState.cardOnTable = Card.fromJson(playerState.cardOnTable);
        if(playerState.trump) playerState.trump = Enum.fromJson(Suit, playerState.trump);
        if(playerState.askedSuit) playerState.askedSuit = Enum.fromJson(Suit, playerState.askedSuit);
        playerState.playerList = playerState.playerList.map(player => Player.fromJson(player));
        playerState.cardsInHand = playerState.cardsInHand.map(card => Card.fromJson(card));
        return playerState;
    }
}

export { Player, PlayerState, Status };
