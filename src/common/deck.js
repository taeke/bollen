import { Enum } from "./enum.js";

/*
Classes for modeling a deck of playing cards.
*/
class Suit {
    static Diamonds = new Suit("Diamonds");
    static Clubs = new Suit("Clubs");   // klavers
    static Hearts = new Suit("Hearts");
    static Spades = new Suit("Spades"); // schoppen

    constructor(name) {
        this.name = name;
    }
}

class Rank {
    static Two = new Rank("Two", "2");
    static Three = new Rank("Three", "3");
    static Four = new Rank("Four", "4");
    static Five = new Rank("Five", "5");
    static Six = new Rank("Six", "6");
    static Seven = new Rank("Seven", "7");
    static Eight = new Rank("Eight", "8");
    static Nine = new Rank("Nine", "9");
    static Ten = new Rank("Ten", "10");
    static Jack = new Rank("Jack", "J");
    static Queen = new Rank("Queen", "Q");
    static King = new Rank("King", "K");
    static Ace = new Rank("Ace", "A");

    constructor(name, shortName) {
        this.shortName = shortName;
        this.name = name;
    }

    isRankedHigher(otherRank) {
        const values =  Object.values(Rank);
        const originalIndex = values.findIndex(rank => rank.name == this.name);
        const otherIndex = values.findIndex(rank => rank.name == otherRank.name)
        return originalIndex > otherIndex;
    }

    static getRankForNumberOfCards(numberOfCards) {
        const values =  Object.values(Rank);
        return values[13 - numberOfCards];
    }
}

class Card {
    constructor(rank, suit) {
        this.suit = suit;
        this.rank = rank;
    }

    static getWinnerIndex(cards, trump, asked) {
        let winnerIndex = 0;
        for (let index = 1; index < cards.length; index++) {
            const winnerSuit = cards[winnerIndex].suit;
            const currentSuit = cards[index].suit;
            if(currentSuit != trump && currentSuit != asked) continue;
            if(winnerSuit == trump && currentSuit != trump) continue;
            if(winnerSuit != trump && currentSuit == trump) {
                winnerIndex = index;
                continue;
            }
            if(winnerSuit != asked && currentSuit == asked) {
                winnerIndex = index;
                continue;
            }

            if(cards[index].rank.isRankedHigher(cards[winnerIndex].rank)) winnerIndex = index;
        }

        return winnerIndex;
    }

    static fromJson(json) {
        return new Card(Enum.fromJson(Rank, json.rank), Enum.fromJson(Suit, json.suit));
    }
}

class Deck {
    static createDeck(fromRank) {
        const suitValues = Object.values(Suit);
        const rankValues = Object.values(Rank);
        const startIndexRank = rankValues.findIndex(rank => rank == fromRank);
        const cards = []

        for (let i = 0; i < suitValues.length; i++) {
            for (let j = startIndexRank; j < rankValues.length; j++) {
                let card = new Card(rankValues[j], suitValues[i]);
                cards.push(card);
            }
        }

        return cards;
    }

    static shuffle(fromRank) {
        console.log(fromRank);
        const cards = Deck.createDeck(fromRank);
        for (let i = cards.length - 1; i > 0; i--) {
            let j = Math.floor(Math.random() * i);
            let temp = cards[i];
            cards[i] = cards[j];
            cards[j] = temp;
        }

        return cards;
    }
}

export { Suit, Rank, Card, Deck };
