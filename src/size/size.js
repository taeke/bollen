import { HandSize } from "./handsize.js";
import { BidListSize } from "./bidlistsize.js";
import { CardSize } from "./cardsize.js";
import { CanvasSize } from "./canvassize.js";
import { RankSize } from "./ranksize.js";
import { TrumpSize } from "./trumpsize.js";
import { AskedSize } from "./askedsize.js";
import { PlayerSize } from "./playersize.js";
import { TableSize } from "./tablesize.js";

/*
Classes for sizing the elements to be drawn on the canvas in relation to the size of the canvas.
*/

class Size {
    constructor(canvasElement) {
        this.canvasElement = canvasElement;
        this.base = undefined;
        this.handSize = undefined;
        this.cardSize = undefined;
        this.bidListSize = undefined;
    }

    calculateBase() {
        if (this.canvasElement.width/36 > this.canvasElement.height / 64) {
            return Math.trunc(this.canvasElement.height / 64);
        }
        else
        {
            return Math.trunc(this.canvasElement.width / 36);
        }
    }

    calculate() {
        this.base = this.calculateBase();
        this.canvasSize = new CanvasSize(this.base, this.canvasElement);
        this.cardSize = new CardSize(this.base);
        this.bidListSize = new BidListSize(this.base, this.canvasSize);
        this.handSize = new HandSize(this.base, this.cardSize, this.canvasSize);
        this.rankSize = new RankSize(this.base);
        this.trumpSize = new TrumpSize(this.base, this.canvasSize);
        this.askedSize = new AskedSize(this.base, this.canvasSize);
        this.fontSize = this.base * 2;
        this.player = new PlayerSize(this.base);
        this.table = new TableSize(this.base, this.canvasSize);
    }
}

export { Size };
