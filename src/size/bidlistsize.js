import { Point, Rectangle } from "../canvas/rectangle.js";

class BidListSize {
    constructor(base, canvasSize) {
        this.bidDistance = base * 7;
        this.topSpace = base * 15;
        this.radius = base * 2;
        this.canvasSize = canvasSize;
    }

    getPoints(possibleBids) {
        const points = [];
        const totalWidth = (possibleBids.length - 1) * this.bidDistance;
        const startX = this.canvasSize.middlePoint.x - Math.trunc(totalWidth / 2);
        for (let index = 0; index < possibleBids.length; index++) {
            const rightX = startX + index * (this.bidDistance);
            const point = new Point(rightX, this.topSpace);
            points.push(point);            
        }

        return points;
    }

    getSlots(possibleBids) {
        const slots = [];
        const totalWidth = (possibleBids.length - 1) * this.bidDistance;
        const startX = this.canvasSize.middlePoint.x - Math.trunc(totalWidth / 2) - this.radius;
        const topY = this.topSpace - this.radius;
        const bottomY = topY + 2 * this.radius;
        for (let index = 0; index < possibleBids.length; index++) {
            const rightX = startX + index * this.bidDistance;
            const slot = new Rectangle(new Point(rightX, topY), new Point(rightX + 2 * this.radius, bottomY));
            slots.push(slot);
        }

        return slots;        
    }
}

export { BidListSize };
