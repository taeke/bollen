import { Point, Rectangle } from "../canvas/rectangle.js";

class HandSize {
    constructor(base, cardSize, canvasSize) {
        this.slotWidth = base * 6;
        this.bottomSpace = base * 4;
        this.cardSize = cardSize;
        this.canvasSize = canvasSize;
    }

    getSlots(numberOfCards) {
        if(numberOfCards <= 0) return;
        const slots = [];
        const totalWidth = (numberOfCards -1 ) * this.slotWidth + this.cardSize.width;
        const startX = this.canvasSize.middlePoint.x - Math.round(totalWidth / 2);
        const bottomY = this.canvasSize.rectangle.bottomRightPoint.y - this.bottomSpace;
        const topY = bottomY - this.cardSize.height;
        for (let index = 0; index < numberOfCards; index++) {
            const rightX = startX + index * this.slotWidth;
            const slot = new Rectangle(new Point(rightX, topY), new Point(rightX + this.slotWidth, bottomY));
            slots.push(slot);
        }

        const lastSlot = slots[slots.length - 1]
        lastSlot.bottomRightPoint.x =  lastSlot.topLeftPoint.x + this.cardSize.width;

        return slots;
    }
}

export { HandSize };
