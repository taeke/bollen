import { Point, Rectangle } from "../canvas/rectangle.js";

class CanvasSize {
    constructor(base, canvasElement) {
        this.rectangle = new Rectangle(new Point(0,0), new Point(canvasElement.width, canvasElement.height));
        this.middlePoint = new Point(Math.trunc(canvasElement.width/ 2), Math.trunc(canvasElement.height/ 2));
    }
}

export { CanvasSize };