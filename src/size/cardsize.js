import { Point } from "../canvas/rectangle.js";

class CardSize {
    constructor(base) {
        this.width = base * 10;
        this.height = base * 15;
        this.radius = base;
    }

    getPoints() {
        const points = [];
        points.push(new Point(0, 0));
        points.push(new Point(this.radius, 0));
        points.push(new Point(this.width, 0));
        points.push(new Point(this.width, this.height));
        points.push(new Point(0, this.height));
        return points;
    }
}

export { CardSize };