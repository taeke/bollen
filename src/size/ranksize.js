import { Point } from "../canvas/rectangle.js";

class RankSize {
    constructor(base) {
        this.base = base;
    }

    getRankGrid() {
        let cellDistHor = Math.trunc(this.base / 1.6);
        let cellDistVer = Math.trunc(this.base/ 1.3);
        let rankGrid = [];
        rankGrid.push([new Point(cellDistHor * 5, cellDistVer * 4), new Point(cellDistHor * 8, cellDistVer * 4), new Point(cellDistHor * 11, cellDistVer * 4)]); // 1
        rankGrid.push([new Point(cellDistHor * 8, cellDistVer * 6)]); // 2
        rankGrid.push([new Point(cellDistHor * 8, cellDistVer * 7)]); // 3
        rankGrid.push([new Point(cellDistHor * 5, cellDistVer * 8), new Point(cellDistHor * 11, cellDistVer * 8)]); // 4
        rankGrid.push([new Point(cellDistHor * 5, cellDistVer * 10), new Point(cellDistHor * 8, cellDistVer * 10), new Point(cellDistHor * 11, cellDistVer * 10)]); // 5
        rankGrid.push([new Point(cellDistHor * 5, cellDistVer * 12), new Point(cellDistHor * 11, cellDistVer * 12)]); // 6
        rankGrid.push([new Point(cellDistHor * 8, cellDistVer * 14)]); // 7
        rankGrid.push([new Point(cellDistHor * 5, cellDistVer * 16), new Point(cellDistHor * 8, cellDistVer * 16), new Point(cellDistHor * 11, cellDistVer * 16)]); // 8
        return rankGrid;
    }

    getTextGrid() {
        let textGrid = [];
        textGrid.push(new Point(Math.trunc(this.base / 0.8), Math.trunc(this.base / 0.8)));
        textGrid.push(new Point(Math.trunc(7 * this.base / 0.8), Math.trunc(this.base / 0.8)));
        textGrid.push(new Point(Math.trunc(this.base / 0.8), Math.trunc(11 * this.base / 0.8)));
        textGrid.push(new Point(Math.trunc(7 * this.base / 0.8), Math.trunc(11 * this.base / 0.8)));
        return textGrid;
    }
}

export { RankSize };
