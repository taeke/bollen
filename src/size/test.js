import { AskedSize } from "./askedsize.js";

const askedSize = new AskedSize();

import { BidListSize } from "./bidlistsize.js";

const bidListSize = new BidListSize();

import { CanvasSize } from "./canvassize.js";

//const canvasSize = new CanvasSize();

import { CardSize } from "./cardsize.js";

const cardSize = new CardSize();

import { HandSize } from "./handsize.js";

const handSize = new HandSize();

import { RankSize } from "./ranksize.js";

const rankSize = new RankSize();

import { Size } from "./size.js";

const size = new Size();

import { TableSize } from "./tablesize.js";

const tableSize = new TableSize();

import { TrumpSize } from "./trumpsize.js";

const trumpSize = new TrumpSize();

import { PlayerSize } from "./playersize.js";

const playerSize = new PlayerSize(10);
console.log(playerSize.getNamePoint());
console.log(playerSize.getBollenPointList(3));
console.log(playerSize.getCardPoint());
