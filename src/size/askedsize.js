import { Point } from "../canvas/rectangle.js";

class AskedSize {
    constructor(base, canvasSize) {
        this.bottomSpace = base * 2;
        this.textX = base * 5;
        this.symbolX = base * 13;
        this.canvasSize = canvasSize;
    }

    getPointText() {
        return new Point(this.canvasSize.middlePoint.x + this.textX, this.canvasSize.rectangle.bottomRightPoint.y - this.bottomSpace);
    }

    getPointSuit() {

        return new Point(this.canvasSize.middlePoint.x + this.symbolX, this.canvasSize.rectangle.bottomRightPoint.y - this.bottomSpace);
    }
}

export { AskedSize };
