import { Point } from "../canvas/rectangle.js";

class PlayerSize {
    constructor(base) {
        this.base = base;
        this.bolSize = base;
    }

    getNamePoint() {
        return new Point(6 * this.base, 2 * this.base);
    }

    getBollenPointList(numberOfBollen) {
        const bollenPointList = [];
        const startX = 6 * this.base - Math.trunc(((numberOfBollen - 1) * this.base));
        for (let index = 0; index < numberOfBollen; index++) {
            bollenPointList.push(new Point(startX + index * this.base * 2, 4 * this.base));
        }

        return bollenPointList;
    }

    getPreviousCardSuitPoint() {
        return new Point(4 * this.base, 7 * this.base);
    }

    getPreviousCardRankPoint() {
        return new Point(7 * this.base, 7 * this.base);
    }

    getCardPoint() {
        return new Point(this.base, 9 * this.base);
    }
}

export { PlayerSize };
