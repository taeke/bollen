import { Point, Rectangle } from "../canvas/rectangle.js";

class TableSize {
    constructor(base, canvasSize) {
        this.base = base;
        this.canvasSize = canvasSize;
        this.snapDistance = 4 * base;
    }

    getPlayerPointList() {
        const playerPointList = [];
        const startX = this.canvasSize.middlePoint.x - 18 * this.base;
        for (let row = 0; row < 2; row++) {
            for (let column = 0; column < 3; column++) {
                playerPointList.push(new Point(startX + column * 12 * this.base, row * 24 * this.base))
            }
        }

        return playerPointList;
    }

    getSnapZones() {
        const snapZones = [];
        const startX = this.canvasSize.middlePoint.x - 18 * this.base;
        for (let row = 0; row < 2; row++) {
            for (let column = 0; column < 3; column++) {
                const topLeft = new Point(startX + column * 12 * this.base - this.snapDistance, row * 24 * this.base - this.snapDistance + (9 * this.base));
                snapZones.push(new Rectangle(topLeft, new Point(topLeft.x + 2 * this.snapDistance, topLeft.y + 2 * this.snapDistance)));
            }
        }

        return snapZones;
    }
}

export { TableSize };