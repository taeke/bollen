import { Translator } from "./translator.js";

class BollenConfig {
    constructor(language, serverId) {
        this.language = language;
        this.translator = new Translator(language);
        this.serverId = serverId;
        this.url = window.location.href.replace(window.location.hash, "") + "#" + language;
        this.pingTimeOut = 10000;
        this.pingResponseTimeOut = 13000;
    }

    static create() {
        const parts = window.location.hash.split("_");
        let lang = BollenConfig.getLang(parts);
        if (parts[1] == undefined) {
            return new BollenConfig(lang);
        }

        return new BollenConfig(lang, parts[1]);
    }

    static getLang(parts) {
        if(parts[0].substring(1) == "") {
            BollenConfig.setNlHash();
            return "nl";
        } else {
            return parts[0].substring(1);
        }
    }

    static setNlHash() {
        if(history.pushState) {
            history.pushState(null, null, "#nl");
        }
        else {
            location.hash = "#nl";
        }
    }
}

export { BollenConfig };