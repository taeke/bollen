import { Client } from "./client/client.js";
import { Server } from "./server/server.js";
import "./style.css";
import { View } from "./view/view.js";

class Start {
    run() {
        if (bollenConfig.serverId == undefined) {
            const view = new View("Bollen", true);
            view.show();
            const server = new Server(view);
            server.onNewGameEvent.addListener(() => this.startNewGame());
            server.run();
        } else {
            const view = new View("Bollen", false);
            view.show();
            const client = new Client(view, bollenConfig.serverId);
            client.run();
        }
    }

    startNewGame() {
        location.reload();
    }
}

export { Start };