import { Peer } from "peerjs";
import { Storage } from "./storage.js";
import { Event } from "../common/event.js";
import { Player, Status } from "../common/playerstate.js";
import { DataType, Data } from "../common/data.js"
import { ClientConnection } from "./clientcollectionlist.js";

/* 
Class for receiving connections from clients and sending and receiving data.
*/
class Connector {
    constructor() {
        this.peer = undefined;

        this.storage = new Storage();
        this.clientConnectionList = [];
        this.maxNumberOfConnections = 6;
        this.isGameStarted = false;

        this.onCanJoinEvent = new Event();
        this.onNewPlayerEvent = new Event();
        this.onReconnectingPlayerEvent = new Event();
        this.onReceivedDataEvent = new Event();
        this.onErrorEvent = new Event();
    }

    run() {
        const state = this.storage.getState();
        this.setUpPeer(state);
    }

    clear() {
        this.storage.remove();
    }

    sendState(state) {
        state.playerStateList.forEach((playerState) => this.sendPlayerState(playerState));
        this.storage.store(state);
    }

    sendPlayerState(playerState) {
        playerState.updateVersion();
        const clientConnection = this.clientConnectionList.find((clientConnection) => clientConnection.clientId == playerState?.clientId);
        if(clientConnection?.connection) {
            this.sendToConnection(clientConnection.connection, new Data(DataType.PlayerState, this.peer.id, playerState));
        } 
    }

    sendPong(clientId) {
        const clientConnection = this.clientConnectionList.find((clientConnection) => clientConnection.clientId == clientId);
        if(clientConnection?.connection) {
            this.sendToConnection(clientConnection.connection, new Data(DataType.Pong, this.peer.id));
        } 
    }

    sendToConnection(connection, data) {
        if (connection.open) {
            connection.send(data);
        } else {
            connection.on("open", function () {
                connection.send(data);
            });
        }
    }

    setUpPeer(state) {
        this.peer = new Peer(state.serverId);
        this.peer.on("open", (serverId) => this.updateState(state, serverId));
        this.peer.on("connection", (connection) => this.updateConnections(connection));
        this.peer.on("error", (error) => this.onPeerError(error));
    }

    onPeerError(error) {
        if (error.type = "peer-unavailable") {
            const length = "Could not connect to peer ".length;
            const id = error.message.substring(length);
            this.onErrorEvent.trigger(id);
        } else {
            console.error(error);
        }
    }

    updateState(state, serverId) {
        state.serverId = serverId;
        this.isGameStarted = state.status == Status.GameStarted;
        this.storage.store(state);
        this.resumeConnections(state);
        this.onCanJoinEvent.trigger(state);
    }

    updateConnections(newConnection) {
        newConnection.on("data", (data) => this.onReceivedDataEvent.trigger(data));
        const index = this.clientConnectionList.findIndex((clientConnection) => clientConnection.clientId == newConnection.peer);
        if(index == -1) {
            this.handleUnKnownConnection(newConnection);
        } else {
            this.handleKnownConnection(newConnection, index);
        }
    }

    handleUnKnownConnection(newConnection) {
        if(this.clientConnectionList.length == this.maxNumberOfConnections || this.isGameStarted) {
            this.sendToConnection(newConnection, new Data(DataType.TooMany, this.peer.id));
        } else {
            this.clientConnectionList.push(new ClientConnection(newConnection.peer, newConnection));
            this.isGameStarted = this.clientConnectionList.length == this.maxNumberOfConnections;
            this.onNewPlayerEvent.trigger(new Player(newConnection.peer, newConnection.label, true));
        }
    }

    handleKnownConnection(newConnection, index) {
        this.clientConnectionList[index].connection?.off("data");
        this.clientConnectionList[index] = new ClientConnection(newConnection.peer, newConnection);
        this.onReconnectingPlayerEvent.trigger(new Player(newConnection.peer, newConnection.label, true));
    }

    resumeConnections(state) {
        state.getPlayerList().forEach((player) => {
            this.clientConnectionList.push(new ClientConnection(player.clientId));
        });
    }
}

export { Connector };
