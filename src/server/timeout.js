import { Event } from "../common/event.js";

/*
Classes to warn when a client does not ping within an expected timeframe.
*/
class ExpectedPingTimeOut {
    constructor(clientId, timeOut) {
        this.clientId = clientId;
        this.timeOut = timeOut;
    }
}

class Timeout {
    constructor() {
        this.expectedPingTimeOuts = [];
        this.onExpectedPingTimedout = new Event();
    }

    setupExpectedPingTimeOut(clientId) {
        const expectedPingTimeOut = this.expectedPingTimeOuts.find(expectedPingTimeOut => expectedPingTimeOut.clientId == clientId);
        const newTimeOut = window.setTimeout(() => this.handlePingTimeOut(clientId), bollenConfig.pingResponseTimeOut);
        if(expectedPingTimeOut) {
            window.clearTimeout(expectedPingTimeOut.timeOut);
            expectedPingTimeOut.timeOut = newTimeOut;
        } else {
            this.expectedPingTimeOuts.push(new ExpectedPingTimeOut(clientId, newTimeOut));
        }
    }

    handlePingTimeOut(clientId) {
        this.onExpectedPingTimedout.trigger(clientId);
    }
}

export { Timeout };
