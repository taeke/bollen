class ClientConnection {
    constructor(clientId, connection) {
        this.clientId = clientId;
        this.connection = connection;
    }
}

export { ClientConnection };