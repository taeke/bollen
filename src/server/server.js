import { Connector } from "./connector.js";
import { Client } from "../client/client.js";
import { DataType } from "../common/data.js";
import { Timeout } from "./timeout.js";
import { Status } from "../common/playerstate.js";
import { Enum } from "../common/enum.js";
import { Event } from "../common/event.js";
import { Card } from "../common/deck.js";

/*
Classes for communication between view, state and connector. 
*/
class Server extends Timeout {
    constructor(view) {
        super();
        this.state = undefined;
        this.client = undefined;

        this.onExpectedPingTimedout.addListener((clientId) => this.handleConnectionLost(clientId));

        this.onNewGameEvent = new Event();
        
        this.view = view;
        this.view.onStartGameEvent.addListener(() => this.startGame());
        this.view.onNewGameEvent.addListener(() => this.hostNewGame());
        this.view.onCopyLinkEvent.addListener(() => this.copyLink());

        this.connector = new Connector();
        this.connector.onCanJoinEvent.addListener((state) => this.startClient(state));
        this.connector.onNewPlayerEvent.addListener((player) => this.handleNewPlayer(player));
        this.connector.onReconnectingPlayerEvent.addListener((player) => this.handleReconnectingPlayer(player));
        this.connector.onErrorEvent.addListener((clientId) => this.handleError(clientId));
        this.connector.onReceivedDataEvent.addListener((data) => this.handleData(data));
    }

    run() {
        this.connector.run();
    }

    handleError(clientId) {
        this.state.connectionLost(clientId);
        this.connector.send(this.state);
    }

    handleData(data) {
        const dataType = Enum.fromJson(DataType, data.dataType);
        switch (dataType) {
            case DataType.Ping:
                this.handlePing(data.content);
                break;
            case DataType.PlayCard:
                this.state.playCard(data.clientId, Card.fromJson(data.content));
                this.connector.sendState(this.state);
                break;
            case DataType.PlaceBid:
                this.state.setBid(data.clientId, data.content);
                this.connector.sendState(this.state);
                break;                
            default:
                this.view.hideJoinElement();
                this.view.showStatus(Status.Error);
                this.view.showError("somethingWrong");
        }
    }

    handlePing(clientId) {
        const playerState = this.state.getPlayerState(clientId);
        if(!playerState?.isConnected) {
            this.state.reconnectingPlayer(clientId);
            this.connector.sendState(this.state);
        }

        this.setupExpectedPingTimeOut(clientId);
        this.connector.sendPong(clientId);
    }

    handleConnectionLost(clientId) {
        this.state.connectionLost(clientId);
        this.connector.sendState(this.state);
    }

    startGame() {
        this.connector.isGameStarted = true;
        this.updateView();
        this.state.startGame();
        this.handleStateUpdate();
    }

    handleStateUpdate() {
        this.connector.sendState(this.state);
    }

    hostNewGame() {
        this.connector.clear();
        this.onNewGameEvent.trigger();
    }

    handleNewPlayer(player) {
        this.updateView();
        this.state.addPlayer(player);
        if(this.connector.isGameStarted) {
            this.startGame();
        } else {
            this.connector.sendState(this.state);
        }
    }

    handleReconnectingPlayer(player) {
        this.updateView();
        this.state.reconnectingPlayer(player.clientId);
        this.connector.sendState(this.state);
    }

    updateView() {
        if(this.connector.isGameStarted) {
            this.view.hideStartAndInviteElements();
        } else {
            this.view.showStartAndInviteElements();
        }
    }

    startClient(state) {
        this.state = state;
        if(this.client) {
            this.client.serverId = this.state.serverId;
        } else {
            this.client = new Client(this.view, this.state.serverId);
        }

        this.client.run();
    }

    copyLink() {
        //navigator.clipboard.writeText(bollenConfig.url + "_" + this.state.serverId);
        this.unsecuredCopyToClipboard(bollenConfig.url + "_" + this.state.serverId);
        this.view.showMessage("copiedLink");
    }

    unsecuredCopyToClipboard(text) {
        const textArea = document.createElement("textarea");
        textArea.value = text;
        document.body.appendChild(textArea);
        textArea.focus();
        textArea.select();
        try {
          document.execCommand('copy');
        } catch (err) {
          console.error('Unable to copy to clipboard', err);
        }
        document.body.removeChild(textArea);
      }
}

export { Server };
