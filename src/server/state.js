import { Player, PlayerState, Status } from "../common/playerstate.js";
import { Enum } from "../common/enum.js";
import { Round } from "./round.js"
import { Deck, Card, Rank } from "../common/deck.js";

/*
Class for keeping track of the current state of the game.
*/
class State {
    constructor() {
        this.status = Status.Waiting;
        this.serverId = undefined;
        this.playerStateList = [];
        this.roundList = [];
        this.currentRoundIndex = -1;
        this.currentPlayerIndex = 0;
        this.startPlayerIndex = 0;
    }

    addPlayer(player) {
        const playerState = PlayerState.makeUnique(player, this.getPlayerList(), this.status);
        this.playerStateList.push(playerState);
        this.updatePlayerStateList(playerState);
    }

    getPlayerState(clientId) {
        return this.playerStateList.find(playerState => playerState.clientId == clientId);
    }

    connectionLost(clientId) {
        this.playerStateList.forEach((playerState) => {
            playerState.setIsConnected(clientId, false);
        });      
    }

    reconnectingPlayer(clientId) {
        this.playerStateList.forEach((playerState) => {
            playerState.setIsConnected(clientId, true);
        });
    }

    updatePlayerStateList(newPlayerState) {
        this.playerStateList.forEach(playerState => {
            playerState.playerList = newPlayerState.playerList;
            playerState.status = newPlayerState.status;
        });
    }

    resetConnected() {
        this.playerStateList.forEach((playerState) => {
            playerState.setIsConnected(playerState.clientId, false);
        });
    }

    getPlayerList() {
        return this.playerStateList.map((playerState) => new Player(playerState.clientId, playerState.name, playerState.isConnected));
    }

    startGame() {
        this.roundList = Round.createRoundList(this.playerStateList.length);
        this.status = Status.GameStarted;
        this.playerStateList.forEach((playerState) => playerState.status = Status.GameStarted);
        this.setNextRound();
    }

    setNextPlayer() {
        if(this.areAlleCardsOfRoundPlayed()) {
            this.setNextRound();
        } else {
            if(this.areAllBidsOfRoundPlaced()) {
                this.setNextToPlayCard();
            } else {
                this.setNextToPlaceBid();
            }
        }
    }

    setNextRound() {
        if(this.currentRoundIndex > -1) this.setBollen();
        this.currentRoundIndex++;
        this.resetCardsOnTable();
        this.resetCurrentRound();
        if(this.isGameFinished()) {

        } else {
            this.dealCards();
            this.currentPlayerIndex = this.roundList[this.currentRoundIndex].indexOfPlayerStartingBid;
            const clientId = this.playerStateList[this.currentPlayerIndex].clientId;
            this.playerStateList.forEach(playerState => playerState.setYourTurnToPlaceBid(clientId));
            this.startPlayerIndex = this.currentPlayerIndex;
            this.playerStateList.forEach(playerState => playerState.trump = this.roundList[this.currentRoundIndex].trump);            
        }
    }

    setNextToPlaceBid() {
        this.currentPlayerIndex = this.getNextIndex();
        const clientId = this.playerStateList[this.currentPlayerIndex].clientId;
        this.playerStateList.forEach(playerState => playerState.setYourTurnToPlaceBid(clientId));
    }

    setNextToPlayCard() {
        if(this.areAllCardsOfTrickPlayed()) {
            this.addScore();
            this.startPlayerIndex = this.getWinnerIndex();
            this.currentPlayerIndex = this.startPlayerIndex;
            this.resetCardsOnTable();
        } else {
            this.currentPlayerIndex = this.getNextIndex();
        }
        
        const clientId = this.playerStateList[this.currentPlayerIndex].clientId;
        this.playerStateList.forEach(playerState => playerState.setYourTurnToPlayCard(clientId));
    }

    setBollen() {
        this.addScore();
        this.playerStateList.forEach(playerState => {
            if (playerState.scoreCurrentRound !== playerState.bidCurrentRound) {
                this.addBol(playerState.clientId);
            }
        });
    }

    addBol(clientId) {
        let newBollen = 0;
        this.playerStateList.forEach(playerState => {
            if (playerState.clientId == clientId) {
                playerState.numberOfBollen = playerState.numberOfBollen + 1;
                newBollen = playerState.numberOfBollen;
            }
        });

        this.playerStateList.forEach(playerState => playerState.addBol(clientId, newBollen));
    }

    addScore() {
        const winnerIndex = this.getWinnerIndex();
        const clientId = this.playerStateList[winnerIndex].clientId;
        let newScore = 0;
        this.playerStateList.forEach(playerState => {
            if (playerState.clientId == clientId) {
                playerState.scoreCurrentRound = playerState.scoreCurrentRound + 1;
                newScore = playerState.scoreCurrentRound;
            }
        });

        this.playerStateList.forEach(playerState => playerState.addScore(clientId, newScore));
    }

    getNextIndex() {
        let next = this.currentPlayerIndex + 1;
        if(next == this.playerStateList.length) next = 0;
        return next;
    }

    setBid(clientId, bid) {
        this.playerStateList.forEach(playerState => playerState.setBid(clientId, bid));
        this.setNextPlayer();
    }

    playCard(clientId, card) {
        this.playerStateList.forEach(playerState => playerState.setCard(clientId, card));
        this.setNextPlayer();
    }

    areAllCardsOfTrickPlayed() {
        let allCardsOnTable = true;
        this.playerStateList.forEach(playerState => { if(playerState.cardOnTable == undefined) allCardsOnTable = false; });
        return allCardsOnTable;
    }

    areAlleCardsOfRoundPlayed() {
        let allCardsOfroundPlayed = true;
        this.playerStateList.forEach(playerState => { if(playerState.cardsInHand.length > 0) allCardsOfroundPlayed = false; });
        return allCardsOfroundPlayed;
    }

    areAllBidsOfRoundPlaced() {
        let allBidsPlaced = true;
        this.playerStateList.forEach(playerState => { if(playerState.bidCurrentRound == undefined) allBidsPlaced = false; });
        return allBidsPlaced;
    }

    resetCardsOnTable() {
        this.playerStateList.forEach(playerState => {
            playerState.resetCard();
            playerState.askedSuit = undefined;
        });
    }

    resetCurrentRound() {
        this.playerStateList.forEach(playerState => playerState.setBid(playerState.clientId, undefined));
        this.playerStateList.forEach(playerState => playerState.resetScore(playerState.clientId));
    }

    isGameFinished() {
        return this.currentRoundIndex == this.roundList.length;
    }

    getWinnerIndex() {
        const askedSuit = this.playerStateList[this.startPlayerIndex].cardOnTable.suit;
        const trump = this.playerStateList[this.startPlayerIndex].trump;
        const cards = this.playerStateList.map(playerState => playerState.cardOnTable);
        return Card.getWinnerIndex(cards, trump, askedSuit);
    }

    dealCards() {
        const rank = Rank.getRankForNumberOfCards(Math.trunc(this.playerStateList.length * 5 / 4) + 1);
        const cards = Deck.shuffle(rank);
        const numberOfCards = this.roundList[this.currentRoundIndex].numberOfCards;
        for (let index = 0; index < numberOfCards; index++) {
            this.playerStateList.forEach(playerState => playerState.cardsInHand.push(cards.pop()));
        }
    }

    static fromJson(json) {
        const state = Object.assign(new State, JSON.parse(json));
        state.status = Enum.fromJson(Status, state.status);
        state.playerStateList = state.playerStateList.map(playerState => PlayerState.fromJson(playerState));
        return state;
    }

    toJson() {
        return JSON.stringify(this);
    }
}

export { State };
