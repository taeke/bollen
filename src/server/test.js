import { ClientConnection } from "./clientcollectionlist.js";

const clientConnection = new ClientConnection();

import { Connector } from "./connector.js";

const connector = new Connector();

import { Round } from "./round.js";

const round = new Round();

import { Server } from "./server.js";

const server = new Server();

import { State } from "./state.js";

const state = new State();

import { Storage } from "./storage.js";

const storage = new Storage();

import { Timeout } from "./timeout.js";

const timeout = new Timeout();

