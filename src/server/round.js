import { Suit } from "../common/deck.js"

/*
class with information about each game round.
*/
class Round {
    constructor(numberOfCards, indexOfPlayerStartingBid) {
        this.trump = Round.getRandomSuit();
        this.numberOfCards = numberOfCards;
        this.indexOfPlayerStartingBid = indexOfPlayerStartingBid;
    }

    static createRoundList(numberOfPlayers) {
        const roundList = [];
        let currentPlayerIndex = 0;
        for (let index = 0; index < 5; index++) {
            roundList.push(new Round(index + 1, currentPlayerIndex));
            currentPlayerIndex = Round.getNextPlayerIndex(currentPlayerIndex, numberOfPlayers);           
        }

        roundList.push(new Round(5, currentPlayerIndex));
        currentPlayerIndex = Round.getNextPlayerIndex(currentPlayerIndex, numberOfPlayers);                

        for (let index = 0; index < 5; index++) {
            roundList.push(new Round(5 - index, currentPlayerIndex));
            currentPlayerIndex = Round.getNextPlayerIndex(currentPlayerIndex, numberOfPlayers);           
        }

        return roundList;
    }

    static getNextPlayerIndex(current, numberOfPlayers) {
        return current + 1 == numberOfPlayers ? 0 : current + 1;
    }

    static getRandomSuit() {
        const suitValues = Object.values(Suit);
        const randomSuit = suitValues[Math.floor(Math.random() * 4)];
        return randomSuit;
    }
}

export { Round };
