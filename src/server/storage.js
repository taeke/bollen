import { State } from "./state.js";

/*
Class for storing and retreiving the state from local storage.
*/
class Storage {
    constructor() {
        this.storageName = "serverV1";
    }

    getState() {
        const json = localStorage.getItem(this.storageName);
        if(json) {
            const state = State.fromJson(json);
            state.resetConnected();
            return state;
        }
        
        return new State();
    }

    remove() {
        localStorage.removeItem(this.storageName);
    }

    store(state) {
        localStorage.setItem(this.storageName, state.toJson());
    }
}

export { Storage };
