import { Event } from "../common/event.js";
import { Elements } from "./elements.js";

/*
Class for showing the invite and start html elements.
*/
class StartAndInvite extends Elements {
    constructor() {
        super();
        this.linkElement = undefined;

        this.onStartEvent = new Event();
        this.onCopyLinkEvent = new Event();
    }

    create() {
        this.linkElement = this.createDivElement("row");

        const copyLink = this.createButtonElement(bollenConfig.translator.translate("copyLink"));
        copyLink.addEventListener("click", () => this.onCopyLinkEvent.trigger());

        this.startButtonElement = this.createButtonElement("start");
        this.startButtonElement.addEventListener("click", () => this.onStartEvent.trigger());

        this.linkElement.appendChild(this.startButtonElement);
        this.linkElement.appendChild(copyLink);
    }

    show(contentElement) {
        contentElement.appendChild(this.linkElement);
    }

    hide(contentElement) {
        this.removeChild(contentElement, this.linkElement);
    }
}

export { StartAndInvite };
