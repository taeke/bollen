import { Elements } from "./elements.js";
import { Event } from "../common/event.js";
import { Header } from "./header.js";
import { Join } from "./join.js";
import { PlayerList } from "./playerList.js";
import { StartAndInvite } from "./startandinvite.js";
import { Status } from "../common/playerstate.js";
import { Canvas } from "../canvas/canvas.js";

/*
Class for showing the userinterface.
*/
class View extends Elements {
    constructor(title, showNew) {
        super();

        this.contentElement = undefined;
        this.containerElement = undefined;
        this.errorElement = undefined;
        this.messageElement = undefined;
        this.header = undefined;

        this.title = title;
        this.showNew = showNew;

        this.onPlayerWantsToJoin = new Event();
        this.onStartGameEvent = new Event();
        this.onNewGameEvent = new Event();
        this.onCopyLinkEvent = new Event();
        this.onPlayedCard = new Event();
        this.onPlaceBid = new Event();

        this.errorTimeOut = undefined;
        this.messageTimeOut = undefined;
    }

    show() {
        this.containerElement = this.createDivElement("container");
        
        this.errorElement = this.createDivElement("error");
        this.messageElement = this.createDivElement("message");

        this.containerElement.appendChild(this.errorElement);
        this.containerElement.appendChild(this.messageElement);

        this.header = new Header(this.containerElement, this.showNew);
        this.header.show(this.title);
        this.header.onNewGameEvent.addListener(() => this.onNewGameEvent.trigger());
                
        this.contentElement = this.createDivElement("content");
        this.containerElement.appendChild(this.contentElement);

        this.join = new Join();
        this.join.create();
        this.join.onJoinEvent.addListener((name) => this.onPlayerWantsToJoin.trigger(name));
        this.join.onNameErrorEvent.addListener(() => this.header.showError("nameConstrain"));

        this.playerList = new PlayerList();

        this.startAndInvite = new StartAndInvite();
        this.startAndInvite.create();
        this.startAndInvite.onStartEvent.addListener(() => this.onStartGameEvent.trigger());
        this.startAndInvite.onCopyLinkEvent.addListener(() => this.onCopyLinkEvent.trigger());

        this.canvas = new Canvas(this.contentElement);
        this.canvas.create(this.header);
        this.canvas.onPlayedCard.addListener((card) => this.onPlayedCard.trigger(card));
        this.canvas.onPlaceBid.addListener((bid) => this.onPlaceBid.trigger(bid));
    }

    showJoinElement() {
        this.join.show(this.contentElement);
        this.header.showStatus(Status.Joining);
    }

    hideJoinElement() {
        this.join.hide(this.contentElement);
    }

    showStartAndInviteElements() {
        this.startAndInvite.show(this.contentElement);
    }

    hideStartAndInviteElements() {
        this.startAndInvite.hide(this.contentElement);
    }

    showStatus(status) {
        this.header.showStatus(status);
    }

    showError(error) {
        if(this.errorTimeOut) window.clearTimeout(this.errorTimeOut);
        this.errorElement.innerHTML = bollenConfig.translator.translate(error);
        this.errorElement.style.visibility = "visible"; 
        this.errorTimeOut = setTimeout(() => this.errorElement.style.visibility = "hidden", 3000);
    }

    showMessage(message, playerName) {
        if(this.messageTimeOut) window.clearTimeout(this.messageTimeOut);
        let translatedMessage = bollenConfig.translator.translate(message);
        if(playerName) translatedMessage = translatedMessage.replace("%%", playerName);
        this.messageElement.innerHTML = translatedMessage;
        this.messageElement.style.visibility = "visible"; 
        this.messageTimeOut = setTimeout(() => this.messageElement.style.visibility = "hidden", 3000);
    }

    showPlayers(playerList, status) {
        this.playerList.show(this.contentElement, playerList, status);
    }

    draw(playerState) {
        this.canvas.draw(playerState);
    }
}

export { View };
