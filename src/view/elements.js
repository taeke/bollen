/*
Class for creating basic html elements and a helper method for removing a child node.
*/
class Elements {
    createDivElement(className) {
        const divElement = document.createElement("div");
        divElement.setAttribute("class", className);
        return divElement;
    }

    createH1Element(content) {
        const h1Element = document.createElement("h1");
        h1Element.innerHTML = content;
        return h1Element;
    }

    createPElement(content) {
        const pElement = document.createElement("p");
        pElement.innerHTML = content;
        return pElement;        
    }
    
    createInputElement(placeHolder) {
        const inputElement = document.createElement("input");
        inputElement.setAttribute("type", "text");
        inputElement.setAttribute("placeholder", placeHolder);
        return inputElement;        
    }

    createButtonElement(content) {
        const buttonElement = document.createElement("button");
        buttonElement.innerHTML = content;
        return buttonElement;
    }

    createCanvasElement(header) {
        const canvasElement = document.createElement("canvas");
        canvasElement.setAttribute("class", "playing_field");
        canvasElement.width = window.innerWidth > 800 ? 800 : window.innerWidth;
        canvasElement.height = window.innerHeight - header.height;
        return canvasElement;
    }

    removeChild(parent, childToRemove) {
        if ( Array.prototype.slice.call(parent.childNodes).find((child) => child == childToRemove)) {
            parent.removeChild(childToRemove);
        }
    }
}

export { Elements };