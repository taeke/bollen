import { Elements } from "./elements.js";
import { Event } from "../common/event.js";

/*
Class for showing the header for the user interface.
*/
class Header extends Elements {
    constructor(containerElement, showNew) {
        super();
        this.showNew = showNew;
        //this.statusElement = undefined;
        this.height = undefined;
        this.onNewGameEvent = new Event();
        this.containerElement = containerElement;
        document.body.appendChild(this.containerElement);
    }

    show(title) {
        const titleElement = this.createTitle(title);
        this.containerElement.appendChild(titleElement);
        //this.statusElement = this.createStatus();
        //this.containerElement.appendChild(this.statusElement);
        //this.height = titleElement.clientHeight + this.statusElement.clientHeight;
        this.height = titleElement.clientHeight;
    }

    createTitle(title) {
        const titleElement = this.createDivElement("title");
        const h1Element = this.createH1Element(title);

        titleElement.appendChild(h1Element);

        if(this.showNew) {
            const buttonElment = this.createButtonElement(bollenConfig.translator.translate("newGame"));
            buttonElment.addEventListener("click", () => this.onNewGameEvent.trigger());
            titleElement.appendChild(buttonElment);
        }
        
        return titleElement; 
    }

    createStatus() {
        const statusElement = this.createDivElement("status");
        const pElement = this.createPElement("Status");
        statusElement.appendChild(pElement);
        return statusElement;
    }

    showStatus(status) {
        //this.statusElement.innerHTML = bollenConfig.translator.fromStatus(status);
    }
}

export { Header };
