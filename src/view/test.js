import { Elements } from "./elements.js";

const elements = new Elements();

import { Header } from "./header.js";

const header = new Header();

import { Join } from "./join.js"

const join = new Join();

import { PlayerList } from "./playerList.js";

const playerList = new PlayerList();

import { StartAndInvite } from "./startandinvite.js";

const startAndInvite = new StartAndInvite();

import { View } from "./view.js";

const view = new View();

