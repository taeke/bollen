import { Status } from "../common/playerstate.js";
import { Elements } from "./elements.js";

/*
Class for showing html elements containing all the player names.
*/
class PlayerList extends Elements {
    constructor() {
        super();
        this.playersElement = undefined;
    }

    show(contentElement, playerList, status) {
        if(this.playersElement) {
            contentElement.removeChild(this.playersElement);
            this.playersElement = undefined;
        }

        if(status !== Status.GameStarted) {
            this.playersElement = document.createElement("ul");
            playerList.forEach(player => {
                const playerElement = this.createPlayer(player);
                this.playersElement.appendChild(playerElement);
            });

            contentElement.appendChild(this.playersElement);
        }
}

    createPlayer(player) {
        const liElement =  document.createElement("li");
        const pElement = this.createPElement(player.name);
        if (!player.isConnected) {
            pElement.setAttribute("class", "disconnected");
        }
        
        liElement.appendChild(pElement);
        return liElement;
    }
}

export { PlayerList };
