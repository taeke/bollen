import { Elements } from "./elements.js";
import { Event } from "../common/event.js";

/*
Class for showing html elements to the user allowing it the join the game.
*/
class Join extends Elements {
    constructor() {
        super();

        this.joinElement = undefined;
        
        this.onJoinEvent = new Event();
        this.onNameErrorEvent = new Event();
    }

    create() {
        this.joinElement = this.createDivElement("row");

        const inputElement = this.createInputElement(bollenConfig.translator.translate("yourName"));
        inputElement.setAttribute("class", "name_input");
        inputElement.pattern = "[A-Za-z]{0,8}";
        inputElement.previousValue = "";
        inputElement.addEventListener("input", () => this.checkName(inputElement));

        const buttonELement = this.createButtonElement(bollenConfig.translator.translate("join"));
        buttonELement.addEventListener("click", () => this.handleJoin(inputElement));
        buttonELement.setAttribute("class", "join_btn");

        this.joinElement.appendChild(inputElement);
        this.joinElement.appendChild(buttonELement);
    }

    handleJoin(inputElement) {
        if(inputElement.value.length >= 2) {
            this.onJoinEvent.trigger(inputElement.value);
        } else {
            this.onNameErrorEvent.trigger();
        }
    }

    checkName(inputElement) {
        if (inputElement.checkValidity()) {
            inputElement.previousValue = inputElement.value;
        } else {
            this.onNameErrorEvent.trigger();
            inputElement.value = inputElement.previousValue;
        }
    }


    show(contentElement) {
        contentElement.appendChild(this.joinElement);
    }

    hide(contentElement) {
        this.removeChild(contentElement, this.joinElement);
    }
}

export { Join };
