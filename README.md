# bollen
Play [Oh Hell](https://en.wikipedia.org/wiki/Oh_Hell) online. Or more specific the dutch version called bollen [Bollen explained in dutch](https://nl.everybodywiki.com/Bollen_(kaartspel)).
[bollen](https://taeke.gitlab.io/bollen/)

[Repo](https://gitlab.com/taeke/bollen)

## Links
[webpack getting starting](https://webpack.js.org/guides/getting-started/)
[Blog containing basic CI/CD for gitlab pages](https://timbarclay.medium.com/keep-api-secrets-secret-with-gitlab-ci-and-webpack-235ff149b294)
[peerjs](https://peerjs.com/)
[peerjs npm](https://www.npmjs.com/package/peerjs)
[location hash](https://developer.mozilla.org/en-US/docs/Web/API/Location/hash)
[routing based on location hash](https://dev.to/thedevdrawer/single-page-application-routing-using-hash-or-url-9jh)
[Map (dictionary) in javascript](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map)
[Create a UUID in javascript](https://www.npmjs.com/package/uuid)
[Local storage](https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage)
[Limit chars in a input](https://stackoverflow.com/questions/9841363/how-to-restrict-number-of-characters-that-can-be-entered-in-html5-number-input-f)
[stop listening to events](https://github.com/peers/peerjs/issues/331)
[multi language](https://javascript.plainenglish.io/diy-multilanguage-in-javascript-project-65f8aa91a506)
[Center button](https://coder-coder.com/how-to-center-button-with-html-css/)
[Flexbox layout](https://codepen.io/visualcookie/pen/ZpyaQZ)
[Message box](https://codepen.io/takaneichinose/pen/eZoZxv)
[Alert](https://codepen.io/lancebush/pen/AGemdG)
